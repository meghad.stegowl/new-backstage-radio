package com.backstage.playerManager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.widget.Toast;

import com.backstage.application.Backstage;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.NotificationTarget;
import com.backstage.R;
import com.backstage.data.models.asset.SongDetails;
import com.backstage.util.AppUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import static com.backstage.playerManager.MusicPlayerService.expandedView;
import static com.backstage.playerManager.MusicPlayerService.notification;
import static com.backstage.playerManager.MusicPlayerService.simpleContentView;


public class MediaController implements NotificationManager.NotificationCenterDelegate, SensorEventListener {

    public static boolean shuffleMusic = false, isAutoPlay = true;
    public static int repeatMode = 0;
    private static boolean isPaused = true;
    private static MediaPlayer audioPlayer = null;
    private static int lastTag = 0;
    private static volatile MediaController Instance = null;
    private final Object playerSync = new Object();
    private final Object playerSongDetailSync = new Object();
    private final Object progressTimerSync = new Object();
    private final Object sync = new Object();
    public int currentPlaylistNum;
    private AudioTrack audioTrackPlayer = null;
    private int lastProgress = 0;
    private boolean useFrontSpeaker;
    private boolean playMusicAgain = false;
    private SensorManager sensorManager;
    private Sensor proximitySensor;
    private boolean ignoreProximity;
    private PowerManager.WakeLock proximityWakeLock;
    private Timer progressTimer = null;
    private int ignoreFirstProgress = 0;
    private long lastPlayPcm;
    private long currentTotalPcmDuration;
    public ArrayList<SongDetails> setSongList = new ArrayList<>();

    public static MediaController getInstance() {
        MediaController localInstance = Instance;
        if (localInstance == null) {
            synchronized (MediaController.class) {
                localInstance = Instance;
                if (localInstance == null) {
                    Instance = localInstance = new MediaController();
                }
            }
        }
        return localInstance;
    }

    public static int generateObserverTag() {
        return lastTag++;
    }

    public static void shuffleList(ArrayList<SongDetails> songs) {
        if (MusicPreference.shuffledPlaylist.isEmpty()) {
            ArrayList<SongDetails> songList = new ArrayList<>(songs);
            int n = songList.size();
            Random random = new Random();
            random.nextInt();
            for (int i = 0; i < n; i++) {
                int change = i + random.nextInt(n - i);
                swap(songList, i, change);
            }
            MusicPreference.shuffledPlaylist = songList;
        }
    }

    private static void swap(ArrayList<SongDetails> songList, int i, int change) {
        SongDetails helper = songList.get(i);
        songList.set(i, songList.get(change));
        songList.set(change, helper);
    }

    public void swapQueue(int fromPosition, int toPosition) {
        SongDetails helper = MusicPreference.playlist.get(fromPosition);
        MusicPreference.playlist.set(fromPosition, MusicPreference.playlist.get(toPosition));
        MusicPreference.playlist.set(toPosition, helper);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void didReceivedNotification(int id, Object... args) {
    }

    @Override
    public void newSongLoaded(Object... args) {
    }

    public SongDetails getPlayingSongDetail() {
        return MusicPreference.playingSongDetail;
    }

    //
    public void setPlayingSongDetails(SongDetails mSongDetail) {
        MusicPreference.playingSongDetail = mSongDetail;
    }

    public boolean isPlayingAudio(SongDetails messageObject) {
        return !(audioTrackPlayer == null && audioPlayer == null || messageObject == null || MusicPreference.playingSongDetail == null || MusicPreference.playingSongDetail != null);
    }

    public boolean isAudioPaused() {
        return isPaused;
    }

    public void playNextSong() {
        playNextSong(false);
    }

    public void playPreviousSong() {
        ArrayList<SongDetails> currentPlayList = shuffleMusic ? MusicPreference.shuffledPlaylist : MusicPreference.playlist;
        currentPlaylistNum = MusicPreference.playlist.indexOf(MusicPreference.playingSongDetail);
        currentPlaylistNum--;
        if (currentPlaylistNum < 0) {
            currentPlaylistNum = currentPlayList.size() - 1;
        }
        if (currentPlaylistNum < 0 || currentPlaylistNum >= currentPlayList.size()) {
            return;
        }
        playMusicAgain = true;
        MusicPreference.playingSongDetail.audioProgress = 0.0f;
        MusicPreference.playingSongDetail.audioProgressSec = 0;
        playAudio(currentPlayList.get(currentPlaylistNum));
    }

    private void stopProgressTimer() {
        synchronized (progressTimerSync) {
            if (progressTimer != null) {
                try {
                    progressTimer.cancel();
                    progressTimer = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void stopProximitySensor() {
        if (ignoreProximity) {
            return;
        }
        try {
            useFrontSpeaker = false;
            NotificationManager.getInstance().postNotificationName(NotificationManager.audioRouteChanged, useFrontSpeaker);
            if (sensorManager != null && proximitySensor != null) {
                sensorManager.unregisterListener(this);
            }
            if (proximityWakeLock != null && proximityWakeLock.isHeld()) {
                proximityWakeLock.release();
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public boolean playAudio(final SongDetails mSongDetail) {
        if (mSongDetail == null) {
            return false;
        }
        if ((audioTrackPlayer != null || audioPlayer != null) && MusicPreference.playingSongDetail != null && mSongDetail.getSongId().equals(MusicPreference.playingSongDetail.getSongId())) {
            if (isPaused) {
                resumeAudio(mSongDetail);
            }
            return true;
        }
        cleanupPlayer(!playMusicAgain, false);
        playMusicAgain = false;
        File file = null;
        try {
            audioPlayer = new MediaPlayer();
            audioPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            audioPlayer.setDataSource(mSongDetail.getType() != null && mSongDetail.getType().toLowerCase().equals(Backstage.applicationContext.getResources().getString(R.string.radio).toLowerCase()) ? mSongDetail.getSong() : mSongDetail.getSong());
            audioPlayer.prepareAsync();
            audioPlayer.setOnBufferingUpdateListener((mediaPlayer, percent) -> {
                MusicPreference.playingSongDetail.audioBufferProgress = percent;
                NotificationManager.getInstance().postNotificationName(NotificationManager.audioBuffered, mSongDetail);
            });

            audioPlayer.setOnSeekCompleteListener(mp -> {

            });

            audioPlayer.setOnErrorListener((mp, what, extra) -> {
                MusicPreference.playingSongDetail.audioProgress = 0.0f;
                MusicPreference.playingSongDetail.audioProgressSec = 0;
                if (!MusicPreference.playlist.isEmpty() && MusicPreference.playlist.size() > 1 && isAutoPlay) {
                    playNextSong(true);
                } else {
                    cleanupPlayer(true, true);
                }
//                Toast.makeText(Backstage.applicationContext, "This song is temporary not available please try again later.", Toast.LENGTH_SHORT).show();
                return true;
            });

            audioPlayer.setOnPreparedListener(mediaPlayer -> {
                if (audioPlayer != null) {
                    audioPlayer.start();
                    MusicPreference.playingSongDetail.audioProgressTotalSec = audioPlayer.getDuration() / 1000;
                    startProgressTimer();
                    if (isPaused) {
                        resumeAudio(mSongDetail);
                    }

                    audioPlayer.setOnCompletionListener(mediaPlayer1 -> {
                        MusicPreference.playingSongDetail.audioProgress = 0.0f;
                        MusicPreference.playingSongDetail.audioProgressSec = 0;
                        if (!MusicPreference.playlist.isEmpty() && (repeatMode == 1 || isAutoPlay)) {
                            playNextSong(true);
                        } else {
                            cleanupPlayer(true, true);
                        }
                    });

                    if (AppUtil.isInternetAvailable(Backstage.applicationContext))
                    new ImageFromURL(mSongDetail).execute();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            if (audioPlayer != null) {
                audioPlayer.release();
                audioPlayer = null;
                isPaused = false;
                MusicPreference.playingSongDetail = null;
            }
            return false;
        }
        isPaused = false;
        lastProgress = 0;
        MusicPreference.playingSongDetail = mSongDetail;
        currentPlaylistNum = MusicPreference.playlist.indexOf(MusicPreference.playingSongDetail);
        NotificationManager.getInstance().postNotificationName(NotificationManager.audioDidStarted, mSongDetail);
        if (audioPlayer != null) {
            try {
                if (MusicPreference.playingSongDetail.audioProgress != 0) {
                    int seekTo = (int) (audioPlayer.getDuration() * MusicPreference.playingSongDetail.audioProgress);
                    audioPlayer.seekTo(seekTo);
                }
            } catch (Exception e2) {
                MusicPreference.playingSongDetail.audioProgress = 0;
                MusicPreference.playingSongDetail.audioProgressSec = 0;
            }
        } else if (audioTrackPlayer != null) {
            if (MusicPreference.playingSongDetail.audioProgress == 1) {
                MusicPreference.playingSongDetail.audioProgress = 0;
            }

        }
        if (MusicPreference.playingSongDetail != null) {
            Intent intent = new Intent(Backstage.applicationContext, MusicPlayerService.class);
            Backstage.applicationContext.startService(intent);
        } else {
            Intent intent = new Intent(Backstage.applicationContext, MusicPlayerService.class);
            Backstage.applicationContext.stopService(intent);
        }
        NotificationManager.getInstance().notifyNewSongLoaded(NotificationManager.newaudioloaded, mSongDetail);
        return true;
    }

    private void playNextSong(boolean byStop) {
        ArrayList<SongDetails> currentPlayList = shuffleMusic ? MusicPreference.shuffledPlaylist : MusicPreference.playlist;

        if (repeatMode == 1 && byStop) {
            cleanupPlayer(false, false);
            playAudio(currentPlayList.get(currentPlaylistNum));
            return;
        }
        currentPlaylistNum = MusicPreference.playlist.indexOf(MusicPreference.playingSongDetail);
        currentPlaylistNum++;
        MediaController.repeatMode = 0;
        MusicPreference.setRepeat(Backstage.applicationContext, 0);
        NotificationManager.getInstance().postNotificationName(NotificationManager.repeatChanged, false);
        if (currentPlaylistNum >= currentPlayList.size()) {
            currentPlaylistNum = 0;
            if (byStop && repeatMode == 0) {
                stopProximitySensor();
                if (audioPlayer != null || audioTrackPlayer != null) {
                    if (audioPlayer != null) {
                        try {
                            audioPlayer.stop();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            audioPlayer.release();
                            audioPlayer = null;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (audioTrackPlayer != null) {
                        synchronized (playerSongDetailSync) {
                            try {
                                audioTrackPlayer.pause();
                                audioTrackPlayer.flush();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                audioTrackPlayer.release();
                                audioTrackPlayer = null;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    stopProgressTimer();
                    lastProgress = 0;
                    isPaused = true;
                    MusicPreference.playingSongDetail.audioProgress = 0.0f;
                    MusicPreference.playingSongDetail.audioProgressSec = 0;
                    NotificationManager.getInstance().postNotificationName(NotificationManager.audioPlayStateChanged, MusicPreference.playingSongDetail.getSongId());
                }
                return;
            }
        }
        if (currentPlaylistNum < 0 || currentPlaylistNum >= currentPlayList.size()) {
            return;
        }
        playMusicAgain = true;
        MusicPreference.playingSongDetail.audioProgress = 0.0f;
        MusicPreference.playingSongDetail.audioProgressSec = 0;
        playAudio(currentPlayList.get(currentPlaylistNum));
    }

    public boolean pauseAudio(SongDetails messageObject) {
        stopProximitySensor();
        if (audioTrackPlayer == null && audioPlayer == null || messageObject == null || MusicPreference.playingSongDetail == null || MusicPreference.playingSongDetail != null
                && !MusicPreference.playingSongDetail.getSongId().equals(messageObject.getSongId())) {
            return false;
        }
        stopProgressTimer();
        try {
            if (audioPlayer != null) {
                audioPlayer.pause();
            } else if (audioTrackPlayer != null) {
                audioTrackPlayer.pause();
            }
            isPaused = true;
            NotificationManager.getInstance().postNotificationName(NotificationManager.audioPlayStateChanged, MusicPreference.playingSongDetail.getSongId());
        } catch (Exception e) {
            e.printStackTrace();
            isPaused = true;
            return false;
        }
        return true;
    }

    public boolean resumeAudio(SongDetails messageObject) {
        if (audioTrackPlayer == null && audioPlayer == null || messageObject == null || MusicPreference.playingSongDetail == null || MusicPreference.playingSongDetail != null
                && !MusicPreference.playingSongDetail.getSongId().equals(messageObject.getSongId())) {
            return false;
        }
        try {
            startProgressTimer();
            if (audioPlayer != null) {
                audioPlayer.start();
            } else if (audioTrackPlayer != null) {
                audioTrackPlayer.play();
            }
            isPaused = false;
            NotificationManager.getInstance().postNotificationName(NotificationManager.audioPlayStateChanged, MusicPreference.playingSongDetail.getSongId());
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void stopAudio() {
        stopProximitySensor();
        if (audioTrackPlayer == null && audioPlayer == null || MusicPreference.playingSongDetail == null) {
            return;
        }
        try {
            if (audioPlayer != null) {
                audioPlayer.stop();
            } else if (audioTrackPlayer != null) {
                audioTrackPlayer.pause();
                audioTrackPlayer.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (audioPlayer != null) {
                audioPlayer.release();
                audioPlayer = null;
            } else if (audioTrackPlayer != null) {
                synchronized (playerSongDetailSync) {
                    audioTrackPlayer.release();
                    audioTrackPlayer = null;
                }
            }
        } catch (Exception e) {
        }
        stopProgressTimer();
        isPaused = false;

        Intent intent = new Intent(Backstage.applicationContext, MusicPlayerService.class);
        Backstage.applicationContext.stopService(intent);
    }

    public void removeSong(SongDetails data) {
        MusicPreference.playlist.remove(data);
        if (!MusicPreference.shuffledPlaylist.isEmpty() && shuffleMusic)
            MusicPreference.shuffledPlaylist.remove(data);
    }

    public void addSong(SongDetails data) {
        if (MusicPreference.playlist.contains(data)) removeSong(data);
        MusicPreference.playlist.add(currentPlaylistNum == MusicPreference.playlist.size() ? currentPlaylistNum : currentPlaylistNum + 1, data);
        if (shuffleMusic) MediaController.shuffleList(MusicPreference.playlist);
    }

    private void startProgressTimer() {
        synchronized (progressTimerSync) {
            if (progressTimer != null) {
                try {
                    progressTimer.cancel();
                    progressTimer = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            progressTimer = new Timer();
            progressTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    synchronized (sync) {
                        PlayerUtility.runOnUIThread(() -> {
                            if (MusicPreference.playingSongDetail != null && (audioPlayer != null || audioTrackPlayer != null) && !isPaused) {
                                try {
                                    if (ignoreFirstProgress != 0) {
                                        ignoreFirstProgress--;
                                        return;
                                    }
                                    int progress;
                                    float value;
                                    if (audioPlayer != null) {
                                        progress = audioPlayer.getCurrentPosition();
                                        value = (float) lastProgress / (float) audioPlayer.getDuration();
                                        if (progress <= lastProgress) {
                                            return;
                                        }
                                    } else {
                                        progress = (int) (lastPlayPcm / 48.0f);
                                        value = (float) lastPlayPcm / (float) currentTotalPcmDuration;
                                        if (progress == lastProgress) {
                                            return;
                                        }
                                    }
                                    lastProgress = progress;
                                    MusicPreference.playingSongDetail.audioProgress = value;
                                    MusicPreference.playingSongDetail.audioProgressSec = lastProgress / 1000;
                                    NotificationManager.getInstance().postNotificationName(NotificationManager.audioProgressDidChanged,
                                            MusicPreference.playingSongDetail.getSongId(), value);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }, 0, 500);
        }
    }

    public boolean setPlaylist(ArrayList<SongDetails> allSongsList, SongDetails current) {
        setSongList = allSongsList;
        if (MusicPreference.playingSongDetail == current) {
            return playAudio(current);
        }

        playMusicAgain = !MusicPreference.playlist.isEmpty();
        MusicPreference.playlist.clear();
        if (allSongsList != null && allSongsList.size() > 0) {
            MusicPreference.playlist.addAll(allSongsList);
            MediaController.repeatMode = 0;
            MusicPreference.setRepeat(Backstage.applicationContext, 0);
            NotificationManager.getInstance().postNotificationName(NotificationManager.repeatChanged, false);

            MediaController.shuffleMusic = false;
            MusicPreference.setShuffle(Backstage.applicationContext, false);
            NotificationManager.getInstance().postNotificationName(NotificationManager.shuffleChanged, false);
            if (shuffleMusic) MediaController.shuffleList(MusicPreference.playlist);
        }

        currentPlaylistNum = MusicPreference.playlist.indexOf(current);
        if (currentPlaylistNum == -1) {
            MusicPreference.playlist.clear();
            MusicPreference.shuffledPlaylist.clear();
            return false;
        }
        if (shuffleMusic) {
            currentPlaylistNum = 0;
        }
        return playAudio(current);
    }

    public boolean seekToProgress(SongDetails mSongDetail, float progress) {
        if (audioTrackPlayer == null && audioPlayer == null) {
            return false;
        }
        try {
            if (audioPlayer != null) {
                int seekTo = (int) (audioPlayer.getDuration() * progress);
                audioPlayer.seekTo(seekTo);
                lastProgress = seekTo;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void cleanupPlayer(Context context, boolean notify, boolean stopService) {
        MusicPreference.saveLastSong(context, getPlayingSongDetail());
        MusicPreference.saveLastPosition(context, currentPlaylistNum);
//        MusicPreference.saveLastPath(context, path);
        cleanupPlayer(notify, stopService);
    }

    public void cleanupPlayer(boolean notify, boolean stopService) {
        pauseAudio(getPlayingSongDetail());
        stopProximitySensor();
        if (audioPlayer != null) {
            try {
                audioPlayer.reset();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                audioPlayer.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                audioPlayer.release();
                audioPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (audioTrackPlayer != null) {
            synchronized (playerSongDetailSync) {
                try {
                    audioTrackPlayer.pause();
                    audioTrackPlayer.flush();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    audioTrackPlayer.release();
                    audioTrackPlayer = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        stopProgressTimer();
        isPaused = true;
        if (stopService) {
            Intent intent = new Intent(Backstage.applicationContext, MusicPlayerService.class);
            Backstage.applicationContext.stopService(intent);
        }
    }

    public void setduckvolume() {
        audioPlayer.setVolume(0.10f, 0.10f);
    }

    public void setnormalvolume() {
        audioPlayer.setVolume(1.0f, 1.0f);
    }

    public void setVolume(String duck) {
        if (duck.equals("DUCK")) {
            audioPlayer.setVolume(0.10f, 0.10f);
        } else {
            audioPlayer.setVolume(1.0f, 1.0f);
        }
    }

    public ArrayList<SongDetails> getPlaylist() {
        return MusicPreference.playlist;
    }

//    public static class getPlaybackUrl extends AsyncTask<String, Void, String> {
//
//        @Override
//        protected String doInBackground(String... path) {
//            try {
//                OkHttpClient client = new OkHttpClient();
//                Request request = new Request.Builder()
//                        .url(Const.ASSET_DETAILS_PLAYBACKURL + path[0])
//                        .build();
//                okhttp3.Response response = client.newCall(request).execute();
//                if (response.isSuccessful() && response.body() != null) {
//                    JSONObject responseObject = new JSONObject(response.body().string());
//                    if (responseObject.getBoolean("success")) {
//                        JSONArray data = responseObject.getJSONArray("data");
//                        if (data.length() != 0 && data.getJSONObject(0).has("playbackUrl") && !data.getJSONObject(0).getString("playbackUrl").equals("")) {
//                            return data.getJSONObject(0).getString("playbackUrl");
//                        } else {
//                            return "";
//                        }
//                    } else {
//                        return "";
//                    }
//                } else {
//                    return "";
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                return "";
//            }
//        }
//    }

    public static class ImageFromURL extends AsyncTask<Void, Void, Bitmap> {

        SongDetails mSongDetail;

        public ImageFromURL(SongDetails mSongDetail) {
            this.mSongDetail = mSongDetail;
        }

        protected Bitmap doInBackground(Void... voids) {
            try {
                URL url = new URL(mSongDetail.getSongImage());
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                return BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @SuppressLint("WrongThread")
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            if (bitmap != null && MusicPreference.playingSongDetail != null) {
                final NotificationTarget smallNotificationTarget = new NotificationTarget(
                        Backstage.applicationContext,
                        R.id.asset_image,
                        simpleContentView,
                        notification,
                        5);

//                final Handler handler = new Handler();
//                new Thread(() -> {
                bitmap.compress(Bitmap.CompressFormat.PNG, 1, stream);
//                    handler.post(() ->
                Glide.with(Backstage.applicationContext)
                        .asBitmap()
                        .load(MediaController.getInstance().getPlayingSongDetail().getSongImage())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .centerCrop()
                        .thumbnail(0.25f)
                        .placeholder(R.drawable.app_logo)
                        .error(R.drawable.app_logo)
                        .into(smallNotificationTarget);

//                }).start();

                final NotificationTarget expandedNotificationTarget = new NotificationTarget(
                        Backstage.applicationContext,
                        R.id.asset_image,
                        expandedView,
                        notification,
                        5);

//                final Handler handler1 = new Handler();
//                new Thread(() -> {
                bitmap.compress(Bitmap.CompressFormat.PNG, 1, stream);
//                    handler1.post(() ->
                Glide.with(Backstage.applicationContext)
                        .asBitmap()
                        .load(stream.toByteArray())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .centerCrop()
                        .thumbnail(0.25f)
                        .placeholder(R.drawable.app_logo)
                        .error(R.drawable.app_logo)
                        .into(expandedNotificationTarget);
//                }).start();
            }
        }

    }

}
