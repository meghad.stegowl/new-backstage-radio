package com.backstage.adapters;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.backstage.R;
import com.backstage.application.Backstage;
import com.backstage.data.models.BannerSliderData;

import java.util.ArrayList;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class AdapterMainviewPager extends PagerAdapter {
    Context context;
    View itemView;
    ArrayList<BannerSliderData> bannerSliderData = new ArrayList<>();

    public AdapterMainviewPager(Context context, ArrayList<BannerSliderData> bannerSliderData) {
        this.context = context;
        this.bannerSliderData = bannerSliderData;
    }

    @Override
    public int getCount() {
        return bannerSliderData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        itemView = layoutInflater.inflate(R.layout.row_carousal, container, false);
        ImageView imageView = itemView.findViewById(R.id.asset_image);

        Glide
                .with(Backstage.applicationContext)
                .load(bannerSliderData.get(position).getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.menu_bottom_joint)
                .centerCrop()/*
                .thumbnail(0.25f)
                .transition(withCrossFade())*/
                .error(R.drawable.menu_bottom_joint)
                .into(imageView);

        imageView.setOnClickListener(view -> {
            try {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(bannerSliderData.get(position).getLink()));
                context.startActivity(intent);

            } catch (ActivityNotFoundException e) {
            }
        });
        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(itemView, 0);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
