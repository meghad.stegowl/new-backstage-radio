package com.backstage.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.backstage.R;
import com.backstage.Services.FloatingWidgetService;
import com.backstage.activities.HomeScreen;
import com.backstage.application.Backstage;
import com.backstage.data.models.asset.SongDetails;
import com.backstage.data.models.music.LikeDislikeSongResponse;
import com.backstage.data.remote.APIUtils;
import com.backstage.fragments.NowPlayingFragment;
import com.backstage.playerManager.MediaController;
import com.backstage.util.AppManageInterface;
import com.backstage.util.AppUtil;
import com.backstage.util.Const;
import com.backstage.util.Prefs;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.backstage.adapters.HomeCategoryAdapter.dialog2;

public class AssetListingAdapter extends RecyclerView.Adapter<AssetListingAdapter.ViewHolder> {

    private final FragmentManager fragmentManager;
    private Context context;
    private String type;
    private String shareDetails;
    private ArrayList<SongDetails> data;
    private AppManageInterface appManageInterface;

    private static final int LIST_AD_DELTA = 9;
    private static final int CONTENT = 0;
    private static final int AD = 1;

    public AssetListingAdapter(Context context, FragmentManager fragmentManager, ArrayList<SongDetails> data, String type) {
        this.context = context;
        this.data = data;
        this.type = type;
        this.fragmentManager = fragmentManager;
        appManageInterface = (AppManageInterface) context;
    }

    @NonNull
    @Override
    public AssetListingAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == CONTENT) {
            return new AssetListingAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_songs, parent, false), viewType);
        } else {
            return new AssetListingAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_ads, parent, false), viewType);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull AssetListingAdapter.ViewHolder viewHolder, int position) {
        AssetListingAdapter.ViewHolder holder = (AssetListingAdapter.ViewHolder) viewHolder;
        if (getItemViewType(position) == CONTENT) {
            final int pos = getRealPosition(position);

            holder.like.setSelected(data.get(pos).getLikesStatus());
            holder.likeCount.setSelected(data.get(pos).getLikesStatus());

            Glide
                    .with(Backstage.applicationContext)
                    .load(data.get(pos).getSongImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .placeholder(R.drawable.app_logo)
                    .error(R.drawable.app_logo)
                    .into(holder.assetImage);

            holder.assetTitle.setText(data.get(pos).getSongName());
            holder.assetArtist.setText(data.get(pos).getSongArtist());
            holder.likeCount.setText(String.valueOf(data.get(pos).getLikesCount()));

            GifDrawable gifdrawable = (GifDrawable) holder.song_small_gif.getDrawable();
            gifdrawable.stop();
            String currentsongurl = String.valueOf(data.get(pos).getSongId());
            String nowpl = Prefs.getPrefInstance().getValue(context, Const.NOW_PLAYING, "");
            if (currentsongurl.equals(nowpl)) {
                holder.song_small_gif.setVisibility(View.VISIBLE);
                holder.iv_play.setVisibility(View.GONE);
                gifdrawable.start();
            } else {
                holder.song_small_gif.setVisibility(View.GONE);
                holder.iv_play.setVisibility(View.VISIBLE);
                gifdrawable.stop();
            }

            holder.container.setOnClickListener(view -> {
                if (dialog2 != null && dialog2.isShowing()) {
                    dialog2.dismiss();
                }

                FloatingWidgetService.hidePipPlayer();
                appManageInterface.closeVideoBottom();
                appManageInterface.bottomanduppermanage(false);
                HomeScreen.isMusicopen = true;
                HomeScreen.isVideoOpen = false;
                appManageInterface.showFragment();
                NowPlayingFragment fragment = NowPlayingFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString("path", data.get(pos).getMenuId());
                bundle.putString("type", "");
                bundle.putString("title", "");
                bundle.putString("id", data.get(pos).getSongId());
                fragment.setArguments(bundle);
                AppUtil.setup_Fragment_with_bundle(fragmentManager, fragment, "Inner", "Inner", true);
                MediaController.getInstance().setPlaylist(data, data.get(holder.getAdapterPosition()));
            });

            holder.like.setOnClickListener(view -> add_remove_like_videos(pos, view, holder));
            shareDetails = "Song Name: " + data.get(pos).getSongName() + "\n\n" + "Song URL: " + data.get(pos).getSong() + "\n\n" +
                    "Artist Name: " + data.get(pos).getSongArtist() + "\n\n" + "App Name: " + context.getResources().getString(R.string.app_name);
            holder.share.setOnClickListener(view -> {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, shareDetails);
                sendIntent.setType("text/plain");
                Intent shareIntent = Intent.createChooser(sendIntent, null);
                context.startActivity(shareIntent);
                appManageInterface.callTotalShared(data);
            });
            holder.assetOptions.setOnClickListener(view -> {
                Log.d("mytag", "adapter type - " + type);
                appManageInterface.showAssetOptions(data.get(pos), type);
            });

        }else{
            AdView adView = new AdView(context);
            adView.setAdSize(AdSize.BANNER);
            adView.setAdUnitId(Prefs.getPrefInstance().getValue(context, Const.Google_add_key, ""));
//            Log.d("mytag", "prefr banner add id is ---" + Prefs.getPrefInstance().getValue(context, Const.Google_add_key, ""));
//            adView.setAdUnitId("ca-app-pub-3940256099942544/6300978111");

            AdRequest adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);

            holder.adViewContainer.addView(adView);

            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    holder.adViewContainer.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAdClosed() {
                    holder.adViewContainer.setVisibility(View.GONE);
                }

                @Override
                public void onAdOpened() {
                    super.onAdOpened();
                }
            });
        }

    }

    private int getRealPosition(int position) {
        if (LIST_AD_DELTA == 0) {
            return position;
        } else {
            return position - position / LIST_AD_DELTA;
        }
    }

    @Override
    public int getItemCount() {
        int additionalContent = 0;
        if (data.size() > 0 && LIST_AD_DELTA > 0 && data.size() > LIST_AD_DELTA) {
            additionalContent = data.size() / LIST_AD_DELTA;
        }
        return data.size() + additionalContent;
    }

    @Override
    public int getItemViewType(int position) {
        if (position > 0 && position % LIST_AD_DELTA == 0) {
            return AD;
        }
        return CONTENT;
    }


    public void add(ArrayList<SongDetails> listingData) {
        this.data.addAll(listingData);
        notifyDataSetChanged();
    }

    public void clear() {
        int size = data.size();
        data.clear();
        notifyItemRangeRemoved(0, size);
    }

    private void add_remove_like_videos(int position, View mainView, AssetListingAdapter.ViewHolder holder) {
        if (AppUtil.isInternetAvailable(context)) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("song_id", data.get(position).getSongId());
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody call_like_dis_like = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().call_like_dis_like(call_like_dis_like).enqueue(new Callback<LikeDislikeSongResponse>() {
                @Override
                public void onResponse(@NonNull Call<LikeDislikeSongResponse> call, @NonNull Response<LikeDislikeSongResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            holder.likeCount.setText(String.valueOf(response.body().getLikesCount()));
                            holder.like.setSelected(response.body().getLikesStatus());
                            holder.likeCount.setSelected(response.body().getLikesStatus());
                            AppUtil.show_Snackbar(context, mainView, response.body().getMessage(), false);
                        } else {
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });

                        }
                    } else {
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<LikeDislikeSongResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView assetTitle;
        TextView assetArtist;
        TextView likeCount;
        ImageView assetImage;
        ImageView like, iv_play;
        ImageView share;
        ImageView assetOptions;
        CardView container;
        GifImageView song_small_gif;
        private LinearLayout adViewContainer;

        ViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);
            assetTitle = itemView.findViewById(R.id.asset_title);
            assetArtist = itemView.findViewById(R.id.asset_artist);
            assetImage = itemView.findViewById(R.id.asset_image);
            share = itemView.findViewById(R.id.share);
            like = itemView.findViewById(R.id.like);
            iv_play = itemView.findViewById(R.id.iv_play);
            likeCount = itemView.findViewById(R.id.like_count);
            assetOptions = itemView.findViewById(R.id.asset_option);
            container = itemView.findViewById(R.id.container);
            song_small_gif = itemView.findViewById(R.id.song_small_gif);
            adViewContainer = itemView.findViewById(R.id.adViewContainer);
        }
    }
}
