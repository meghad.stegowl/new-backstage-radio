package com.backstage.adapters;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.backstage.application.Backstage;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.backstage.R;
import com.backstage.data.models.HomeCategoryData;
import com.backstage.data.models.asset.SongDetails;
import com.backstage.fragments.NowPlayingFragment;
import com.backstage.playerManager.MediaController;
import com.backstage.util.AppManageInterface;
import com.backstage.util.AppUtil;

import java.util.ArrayList;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class HomeMusicAdapter extends RecyclerView.Adapter<HomeMusicAdapter.ViewHolder> {

    private Context context;
    private ArrayList<HomeCategoryData> homeCategoryData;
    private FragmentManager fragmentManager;
    private ArrayList<SongDetails> songDetails = new ArrayList<>();
    private AppManageInterface appManageInterface;

    public HomeMusicAdapter(Context context, FragmentManager fragmentManager, ArrayList<HomeCategoryData> homeCategoryData) {
        this.context = context;
        this.appManageInterface = (AppManageInterface) context;
        this.homeCategoryData = homeCategoryData;
        this.fragmentManager = fragmentManager;

        try {
            for (int i = 0; i < homeCategoryData.size(); i++) {
                SongDetails addvpsong = new SongDetails(String.valueOf(homeCategoryData.get(i).getSongId()),
                        String.valueOf( homeCategoryData.get(i).getMenuId()),
                        homeCategoryData.get(i).getSongImage(),
                        homeCategoryData.get(i).getSongName(), homeCategoryData.get(i).getSongArtist(),
                        homeCategoryData.get(i).getSong(), homeCategoryData.get(i).getSongDuration());
                songDetails.add(addvpsong);
            }
        } catch (Exception e) {

        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_home_music, parent, false);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        layoutParams.width = (int) (parent.getWidth() / 4);
        view.setLayoutParams(layoutParams);
        return new HomeMusicAdapter.ViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tv_song_name.setText(homeCategoryData.get(position).getSongName());
        holder.tv_artist_name.setText(homeCategoryData.get(position).getSongArtist());
        Glide
                .with(Backstage.applicationContext)
                .load(homeCategoryData.get(position).getSongImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.25f)
                .centerCrop()
                .placeholder(R.drawable.app_logo)
                .into(holder.iv_music);
        Log.d("mytag","here is image is---------------------"+homeCategoryData.get(position).getSongImage());
        Log.d("mytag","here is image is---------------------"+homeCategoryData.get(position).getSongName());
        Log.d("mytag","here is image is---------------------"+homeCategoryData.get(position).getSongArtist());

        holder.rl_music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SongDetails data = songDetails.get(position);
                NowPlayingFragment fragment = NowPlayingFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString("path", String.valueOf(songDetails.get(position).getMenuId()));
                bundle.putString("type", "");
                bundle.putString("title", "");
                bundle.putString("id", String.valueOf(songDetails.get(position).getSongId()));
                fragment.setArguments(bundle);
                AppUtil.setup_Fragment_with_bundle(fragmentManager, fragment, "Inner", "Inner", true);
                try {
                    MediaController.getInstance().setPlaylist(songDetails, songDetails.get(holder.getAdapterPosition()));
                }catch (Exception e){

                }
               appManageInterface.showFragment();
            }
        });
    }

    @Override
    public int getItemCount() {
        return homeCategoryData.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_song_name,tv_artist_name;
        RelativeLayout rl_music;
        ImageView iv_music;

        ViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);
            rl_music = itemView.findViewById(R.id.rl_music);
            tv_song_name = itemView.findViewById(R.id.tv_song_name);
            tv_artist_name = itemView.findViewById(R.id.tv_artist_name);
            iv_music = itemView.findViewById(R.id.iv_music);
        }
    }
}
