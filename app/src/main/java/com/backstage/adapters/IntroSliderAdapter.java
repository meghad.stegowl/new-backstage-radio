package com.backstage.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.backstage.R;
import com.backstage.activities.Login;
import com.backstage.data.models.intro_slider.IntroSliderData;
import com.backstage.util.AppUtil;
import com.backstage.util.Const;
import com.backstage.util.Prefs;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class IntroSliderAdapter extends PagerAdapter {


    @BindView(R.id.slider_image)
    ImageView sliderImage;
    @BindView(R.id.skip)
    LinearLayout skip;
    @BindView(R.id.slider_title)
    TextView sliderTitle;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.next)
    TextView next;
    @BindView(R.id.next_container)
    LinearLayout nextContainer;
    private Context context;
    private ArrayList<IntroSliderData> data;
    ViewPager slider;

    public IntroSliderAdapter(Context context, ArrayList<IntroSliderData> data, ViewPager slider) {
        this.context = context;
        this.data = data;
        this.slider = slider;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NotNull
    @Override
    public Object instantiateItem(@NotNull ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context,
                R.layout.row_intro_slider), null);
        ButterKnife.bind(this, view);

        if (position == data.size() - 1) {
            next.setText("Enter");
        } else {
            next.setText("Next");
        }

        skip.setOnClickListener(view1 -> {
            Log.d("mytag", "skip IntroSlider--------------+" );
            Prefs.getPrefInstance().setValue(context, Const.APP_STATUS, "1");
            context.startActivity(new Intent(context, Login.class).putExtra("isLogin", true));
            ((Activity) context).finish();
        });

        nextContainer.setOnClickListener(view12 -> {
            if (position < data.size() - 1) {
                slider.setCurrentItem(position + 1);
                Log.d("mytag", "nextContainer if in IntroSlider--------------+" );
            } else {
                Log.d("mytag", "nextContainer else in IntroSlider--------------+" );
                Prefs.getPrefInstance().setValue(context, Const.APP_STATUS, "1");
                context.startActivity(new Intent(context, Login.class).putExtra("isLogin", true));
                ((Activity) context).finish();
            }
        });

        sliderTitle.setText(data.get(position).getAppTitle());

        if (data.get(0).getAppImage() != null && !data.get(0).getAppImage().isEmpty()) {
            Glide
                    .with(context)
                    .load(data.get(position).getAppImage())
                    .fitCenter()
                    .transition(withCrossFade())
                    .placeholder(R.drawable.app_logo)
                    .into(sliderImage);
        } else {
            sliderImage.setImageResource(R.drawable.app_logo);
        }

//        indicator.setViewPager(slider);

        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(@NotNull ViewGroup container, int position, @NotNull Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}
