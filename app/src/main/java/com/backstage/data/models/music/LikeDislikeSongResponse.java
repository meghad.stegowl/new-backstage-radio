package com.backstage.data.models.music;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LikeDislikeSongResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("likes_count")
    @Expose
    private Integer likesCount;
    @SerializedName("likes_status")
    @Expose
    private Boolean likesStatus;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(Integer likesCount) {
        this.likesCount = likesCount;
    }

    public Boolean getLikesStatus() {
        return likesStatus;
    }

    public void setLikesStatus(Boolean likesStatus) {
        this.likesStatus = likesStatus;
    }
}
