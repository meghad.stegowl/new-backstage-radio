package com.backstage.data.models.drawer_items;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DrawerItemsDetails {

    @SerializedName("menu_id")
    @Expose
    private String menuId;
    @SerializedName("menu_image")
    @Expose
    private String menuImage;
    @SerializedName("menu_name")
    @Expose
    private String menuName;
    @SerializedName("menu_type")
    @Expose
    private String menuType;
    @SerializedName("livetv_status")
    @Expose
    private String livetvStatus;
    @SerializedName("visible_status")
    @Expose
    private Boolean visibleStatus;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getMenuImage() {
        return menuImage;
    }

    public void setMenuImage(String menuImage) {
        this.menuImage = menuImage;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuType() {
        return menuType;
    }

    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }

    public String getLivetvStatus() {
        return livetvStatus;
    }

    public void setLivetvStatus(String livetvStatus) {
        this.livetvStatus = livetvStatus;
    }

    public Boolean getVisibleStatus() {
        return visibleStatus;
    }

    public void setVisibleStatus(Boolean visibleStatus) {
        this.visibleStatus = visibleStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
