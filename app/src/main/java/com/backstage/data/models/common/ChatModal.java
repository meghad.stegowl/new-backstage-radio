package com.backstage.data.models.common;

public class ChatModal {
    String time;
    String commentDate;
    String commentImageURL;
    String commentByName;
    String comment;
    String type;

    public ChatModal() {
    }

    public ChatModal(String time, String date, String image, String commentByName, String comment, String type) {
        this.time = time;
        this.commentDate = date;
        this.commentImageURL = image;
        this.commentByName = commentByName;
        this.comment = comment;
        this.type = type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return commentDate;
    }

    public void setDate(String date) {
        this.commentDate = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getImage() {
        return commentImageURL;
    }

    public void setImage(String image) {
        this.commentImageURL = image;
    }

    public String getCommentByName() {
        return commentByName;
    }

    public void setCommentByName(String commentByName) {
        this.commentByName = commentByName;
    }
}
