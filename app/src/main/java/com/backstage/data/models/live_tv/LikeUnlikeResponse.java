package com.backstage.data.models.live_tv;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LikeUnlikeResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("videos_like_status")
    @Expose
    private Boolean videosLikeStatus;
    @SerializedName("videos_like_count")
    @Expose
    private String videosLikeCount;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getVideosLikeStatus() {
        return videosLikeStatus;
    }

    public void setVideosLikeStatus(Boolean videosLikeStatus) {
        this.videosLikeStatus = videosLikeStatus;
    }

    public String getVideosLikeCount() {
        return videosLikeCount;
    }

    public void setVideosLikeCount(String videosLikeCount) {
        this.videosLikeCount = videosLikeCount;
    }
}
