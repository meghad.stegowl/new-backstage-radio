package com.backstage.data.models.video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class VideoDetailsResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("videos_detail")
    @Expose
    private ArrayList<VideoDetailsData> videosDetail = null;
    @SerializedName("suggested_data")
    @Expose
    private ArrayList<MenuItemsData> suggestedData = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<VideoDetailsData> getVideosDetail() {
        return videosDetail;
    }

    public void setVideosDetail(ArrayList<VideoDetailsData> videosDetail) {
        this.videosDetail = videosDetail;
    }

    public ArrayList<MenuItemsData> getSuggestedData() {
        return suggestedData;
    }

    public void setSuggestedData(ArrayList<MenuItemsData> suggestedData) {
        this.suggestedData = suggestedData;
    }
}
