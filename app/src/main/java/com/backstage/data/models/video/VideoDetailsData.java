package com.backstage.data.models.video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoDetailsData {

    @SerializedName("categoryitems_id")
    @Expose
    private String categoryitemsId;
    @SerializedName("menu_id")
    @Expose
    private String menuId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("videos_id")
    @Expose
    private String videosId;
    @SerializedName("videos_image")
    @Expose
    private String videosImage;
    @SerializedName("videos_name")
    @Expose
    private String videosName;
    @SerializedName("videos_description")
    @Expose
    private String videosDescription;
    @SerializedName("videos_link")
    @Expose
    private String videosLink;
    @SerializedName("livetv_status")
    @Expose
    private String livetvStatus;
    @SerializedName("favourites_status")
    @Expose
    private Boolean favouritesStatus;
    @SerializedName("favourites_count")
    @Expose
    private Integer favouritesCount;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;

    public String getCategoryitemsId() {
        return categoryitemsId;
    }

    public void setCategoryitemsId(String categoryitemsId) {
        this.categoryitemsId = categoryitemsId;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getVideosId() {
        return videosId;
    }

    public void setVideosId(String videosId) {
        this.videosId = videosId;
    }

    public String getVideosImage() {
        return videosImage;
    }

    public void setVideosImage(String videosImage) {
        this.videosImage = videosImage;
    }

    public String getVideosName() {
        return videosName;
    }

    public void setVideosName(String videosName) {
        this.videosName = videosName;
    }

    public String getVideosDescription() {
        return videosDescription;
    }

    public void setVideosDescription(String videosDescription) {
        this.videosDescription = videosDescription;
    }

    public String getVideosLink() {
        return videosLink;
    }

    public void setVideosLink(String videosLink) {
        this.videosLink = videosLink;
    }

    public String getLivetvStatus() {
        return livetvStatus;
    }

    public void setLivetvStatus(String livetvStatus) {
        this.livetvStatus = livetvStatus;
    }

    public Boolean getFavouritesStatus() {
        return favouritesStatus;
    }

    public void setFavouritesStatus(Boolean favouritesStatus) {
        this.favouritesStatus = favouritesStatus;
    }

    public Integer getFavouritesCount() {
        return favouritesCount;
    }

    public void setFavouritesCount(Integer favouritesCount) {
        this.favouritesCount = favouritesCount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
