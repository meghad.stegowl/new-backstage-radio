package com.backstage.data.models.video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MenuLiveTvData {
    @SerializedName("menu_id")
    @Expose
    private String menuId;
    @SerializedName("visible_status")
    @Expose
    private Boolean visibleStatus;

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public Boolean getVisibleStatus() {
        return visibleStatus;
    }

    public void setVisibleStatus(Boolean visibleStatus) {
        this.visibleStatus = visibleStatus;
    }
}
