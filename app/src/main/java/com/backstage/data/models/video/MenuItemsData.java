package com.backstage.data.models.video;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MenuItemsData {

    @SerializedName("categoryitems_id")
    @Expose
    private String categoryitemsId;
    @SerializedName("menu_id")
    @Expose
    private String menuId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("videos_id")
    @Expose
    private String videosId;
    @SerializedName("videos_image")
    @Expose
    private String videosImage;
    @SerializedName("videos_name")
    @Expose
    private String videosName;
    @SerializedName("videos_description")
    @Expose
    private String videosDescription;
    @SerializedName("videos_link")
    @Expose
    private String videosLink;
    @SerializedName("livetv_status")
    @Expose
    private String livetvStatus;
    @SerializedName("video_like_status")
    @Expose
    private Boolean videoLikeStatus;
    @SerializedName("video_like_count")
    @Expose
    private String videoLikeCount;
    @SerializedName("popup_video_status")
    @Expose
    private Boolean popupVideoStatus;
    @SerializedName("favourites_status")
    @Expose
    private Boolean favouritesStatus;
    @SerializedName("favourites_count")
    @Expose
    private Integer favouritesCount;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("category_image")
    @Expose
    private String categoryImage;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("parent_category_name")
    @Expose
    private String parentCategoryName;
    @SerializedName("category_for")
    @Expose
    private String categoryFor;
    @SerializedName("image_status")
    @Expose
    private Boolean imageStatus;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("url_type")
    @Expose
    private Integer urlType;
    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryFor() {
        return categoryFor;
    }

    public void setCategoryFor(String categoryFor) {
        this.categoryFor = categoryFor;
    }

    public Boolean getImageStatus() {
        return imageStatus;
    }

    public void setImageStatus(Boolean imageStatus) {
        this.imageStatus = imageStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCategoryitemsId() {
        return categoryitemsId;
    }

    public void setCategoryitemsId(String categoryitemsId) {
        this.categoryitemsId = categoryitemsId;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getVideosId() {
        return videosId;
    }

    public void setVideosId(String videosId) {
        this.videosId = videosId;
    }

    public String getVideosImage() {
        return videosImage;
    }

    public void setVideosImage(String videosImage) {
        this.videosImage = videosImage;
    }

    public String getVideosName() {
        return videosName;
    }

    public void setVideosName(String videosName) {
        this.videosName = videosName;
    }

    public String getVideosDescription() {
        return videosDescription;
    }

    public void setVideosDescription(String videosDescription) {
        this.videosDescription = videosDescription;
    }

    public String getVideosLink() {
        return videosLink;
    }

    public void setVideosLink(String videosLink) {
        this.videosLink = videosLink;
    }

    public String getLivetvStatus() {
        return livetvStatus;
    }

    public void setLivetvStatus(String livetvStatus) {
        this.livetvStatus = livetvStatus;
    }

    public Boolean getVideoLikeStatus() {
        return videoLikeStatus;
    }

    public void setVideoLikeStatus(Boolean videoLikeStatus) {
        this.videoLikeStatus = videoLikeStatus;
    }

    public String getVideoLikeCount() {
        return videoLikeCount;
    }

    public void setVideoLikeCount(String videoLikeCount) {
        this.videoLikeCount = videoLikeCount;
    }

    public Boolean getPopupVideoStatus() {
        return popupVideoStatus;
    }

    public void setPopupVideoStatus(Boolean popupVideoStatus) {
        this.popupVideoStatus = popupVideoStatus;
    }

    public Boolean getFavouritesStatus() {
        return favouritesStatus;
    }

    public void setFavouritesStatus(Boolean favouritesStatus) {
        this.favouritesStatus = favouritesStatus;
    }

    public Integer getFavouritesCount() {
        return favouritesCount;
    }

    public void setFavouritesCount(Integer favouritesCount) {
        this.favouritesCount = favouritesCount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getParentCategoryName() {
        return parentCategoryName;
    }

    public void setParentCategoryName(String parentCategoryName) {
        this.parentCategoryName = parentCategoryName;
    }

    public Integer getUrlType() {
        return urlType;
    }

    public void setUrlType(Integer urlType) {
        this.urlType = urlType;
    }
}
