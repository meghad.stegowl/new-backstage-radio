package com.backstage.data.models.video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MenuStatusResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("livetv_menu_data")
    @Expose
    private ArrayList<MenuLiveTvData> livetvMenuData = null;
    @SerializedName("radio_menu_data")
    @Expose
    private ArrayList<MenuRadioData> radioMenuData = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<MenuLiveTvData> getLivetvMenuData() {
        return livetvMenuData;
    }

    public void setLivetvMenuData(ArrayList<MenuLiveTvData> livetvMenuData) {
        this.livetvMenuData = livetvMenuData;
    }

    public ArrayList<MenuRadioData> getRadioMenuData() {
        return radioMenuData;
    }

    public void setRadioMenuData(ArrayList<MenuRadioData> radioMenuData) {
        this.radioMenuData = radioMenuData;
    }
}
