package com.backstage.data.models.intro_slider;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class IntroSliderResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("web_data")
    @Expose
    private ArrayList<WebSliderData> webData = null;
    @SerializedName("app_data")
    @Expose
    private ArrayList<IntroSliderData> appData = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<WebSliderData> getWebData() {
        return webData;
    }

    public void setWebData(ArrayList<WebSliderData> webData) {
        this.webData = webData;
    }

    public ArrayList<IntroSliderData> getAppData() {
        return appData;
    }

    public void setAppData(ArrayList<IntroSliderData> appData) {
        this.appData = appData;
    }
}
