package com.backstage.data.models.share_my_app;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShareMyAppData {

    @SerializedName("android_link")
    @Expose
    private String androidLink;
    @SerializedName("ios_link")
    @Expose
    private String iosLink;

    public String getAndroidLink() {
        return androidLink;
    }

    public void setAndroidLink(String androidLink) {
        this.androidLink = androidLink;
    }

    public String getIosLink() {
        return iosLink;
    }

    public void setIosLink(String iosLink) {
        this.iosLink = iosLink;
    }
}
