package com.backstage.data.models.common;

public class DrawerItems {
    private String title;
    private String header;
    private String path;
    private String menu_type;
    private int type;
    private String ImageUrl;
    private boolean visible_status;
    private String livetv_status;
    private int Image;
    private int res;

    public String getMenu_type() {
        return menu_type;
    }

    public void setMenu_type(String menu_type) {
        this.menu_type = menu_type;
    }

    public boolean isVisible_status() {
        return visible_status;
    }

    public void setVisible_status(boolean visible_status) {
        this.visible_status = visible_status;
    }

    public String getLivetv_status() {
        return livetv_status;
    }

    public void setLivetv_status(String livetv_status) {
        this.livetv_status = livetv_status;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public int getImage() {
        return Image;
    }

    public void setImage(int image) {
        Image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getRes() {
        return res;
    }

    public void setRes(int res) {
        this.res = res;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
