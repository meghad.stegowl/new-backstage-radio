package com.backstage.data.models.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class HomeItemsResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("livetvvideo_data")
    @Expose
    private ArrayList<HomeItemsLiveTvVideoData> livetvvideoData = null;
    @SerializedName("radio_data")
    @Expose
    private ArrayList<HomeItemsRadioData> radioData = null;
    @SerializedName("popupvideo_data")
    @Expose
    private ArrayList<HomeItemsPopupVideoData> popupvideoData = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<HomeItemsLiveTvVideoData> getLivetvvideoData() {
        return livetvvideoData;
    }

    public void setLivetvvideoData(ArrayList<HomeItemsLiveTvVideoData> livetvvideoData) {
        this.livetvvideoData = livetvvideoData;
    }

    public ArrayList<HomeItemsRadioData> getRadioData() {
        return radioData;
    }

    public void setRadioData(ArrayList<HomeItemsRadioData> radioData) {
        this.radioData = radioData;
    }

    public ArrayList<HomeItemsPopupVideoData> getPopupvideoData() {
        return popupvideoData;
    }

    public void setPopupvideoData(ArrayList<HomeItemsPopupVideoData> popupvideoData) {
        this.popupvideoData = popupvideoData;
    }
}
