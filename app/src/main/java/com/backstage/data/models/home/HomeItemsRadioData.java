package com.backstage.data.models.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeItemsRadioData {

    @SerializedName("categoryitems_id")
    @Expose
    private String categoryitemsId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("menu_id")
    @Expose
    private String menuId;
    @SerializedName("radio_id")
    @Expose
    private String radioId;
    @SerializedName("radio_name")
    @Expose
    private String radioName;
    @SerializedName("radio_image")
    @Expose
    private String radioImage;
    @SerializedName("radio_link")
    @Expose
    private String radioLink;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public String getCategoryitemsId() {
        return categoryitemsId;
    }

    public void setCategoryitemsId(String categoryitemsId) {
        this.categoryitemsId = categoryitemsId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getRadioId() {
        return radioId;
    }

    public void setRadioId(String radioId) {
        this.radioId = radioId;
    }

    public String getRadioName() {
        return radioName;
    }

    public void setRadioName(String radioName) {
        this.radioName = radioName;
    }

    public String getRadioImage() {
        return radioImage;
    }

    public void setRadioImage(String radioImage) {
        this.radioImage = radioImage;
    }

    public String getRadioLink() {
        return radioLink;
    }

    public void setRadioLink(String radioLink) {
        this.radioLink = radioLink;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
