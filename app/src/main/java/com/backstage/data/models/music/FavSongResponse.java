package com.backstage.data.models.music;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FavSongResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("favourites_count")
    @Expose
    private Integer favoritesCount;
    @SerializedName("favourites_status")
    @Expose
    private Boolean favoritesStatus;

    public Integer getFavoritesCount() {
        return favoritesCount;
    }

    public void setFavoritesCount(Integer favoritesCount) {
        this.favoritesCount = favoritesCount;
    }

    public Boolean getFavoritesStatus() {
        return favoritesStatus;
    }

    public void setFavoritesStatus(Boolean favoritesStatus) {
        this.favoritesStatus = favoritesStatus;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
