package com.backstage.data.models.music;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExistingPlaylistData {

    @SerializedName("playlist_id")
    @Expose
    private String playlistId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("playlist_name")
    @Expose
    private String playlistName;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public String getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(String playlistId) {
        this.playlistId = playlistId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPlaylistName() {
        return playlistName;
    }

    public void setPlaylistName(String playlistName) {
        this.playlistName = playlistName;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
