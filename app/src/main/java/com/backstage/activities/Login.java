package com.backstage.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.backstage.R;
import com.backstage.data.models.user.LoginUserResponse;
import com.backstage.data.models.user.SkipUserResponse;
import com.backstage.data.remote.APIUtils;
import com.backstage.databinding.ActivityLoginBinding;
import com.backstage.util.AppUtil;
import com.backstage.util.Const;
import com.backstage.util.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    ActivityLoginBinding binding;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        binding.loader.setVisibility(View.GONE);
        init();
    }

    private void init() {
        if (!Prefs.getPrefInstance().getValue(Login.this, Const.MENU_LIVE_TV_STATUS, "").isEmpty()
                && Prefs.getPrefInstance().getValue(Login.this, Const.MENU_LIVE_TV_STATUS, "") != null
                && Prefs.getPrefInstance().getValue(Login.this, Const.MENU_LIVE_TV_STATUS, "".toLowerCase()).equals("visible"))
            binding.liveTv.setVisibility(View.VISIBLE);
        else binding.liveTv.setVisibility(View.GONE);
        binding.emailInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.emailContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.passwordInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.passwordContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if (Prefs.getPrefInstance().getValue(Login.this, Const.KEEP_USER_LOGGED_IN, "").equals("1")) {
            binding.emailInput.setText(Prefs.getPrefInstance().getValue(Login.this, Const.USER_NAME_EMAIL_CHECKED, ""));
            binding.passwordInput.setText(Prefs.getPrefInstance().getValue(Login.this, Const.PASSWORD, ""));
            binding.rememberMe.setChecked(true);
        }
        setTerms_PrivacyText();

        binding.buttonLogin.setOnClickListener(view -> {
            if (binding.emailInput.getText() == null) {
                binding.emailContainer.setError(getResources().getString(R.string.email_username_empty));
            } else if (binding.emailInput.getText().toString().trim().length() == 0) {
                binding.emailContainer.setError(getResources().getString(R.string.email_username_empty));
            } else if (binding.passwordInput.getText() == null) {
                binding.passwordContainer.setError(getResources().getString(R.string.password_empty));
            } else if (binding.passwordInput.getText().toString().trim().length() == 0) {
                binding.passwordContainer.setError(getResources().getString(R.string.password_empty));
            } else {
                callLogIn(binding.emailInput.getText().toString(), binding.passwordInput.getText().toString());
            }
        });

        binding.buttonRegister.setOnClickListener(view -> {
            Intent i = new Intent(Login.this, SignUp.class);
            startActivity(i);
            finish();
        });
        binding.home.setOnClickListener(view -> {
            callSkipUser();
        });
        binding.forgotPassword.setOnClickListener(view -> {
            Intent i = new Intent(Login.this, ForgotPassword.class);
            startActivity(i);
            finish();
        });
        binding.buttonGuestLogin.setOnClickListener(view -> {
            callSkipUser();
        });
        binding.liveTv.setOnClickListener(view -> startActivity(new Intent(Login.this, LiveTv.class).putExtra("type", "Videos").putExtra("path", Prefs.getPrefInstance().getValue(Login.this, Const.MENU_LIVE_TV_ID, ""))
                .putExtra("title", Login.this.getResources().getString(R.string.live_tv))));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    private void callLogIn(String email, String password) {
        if (AppUtil.isInternetAvailable(Login.this)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                if (Patterns.EMAIL_ADDRESS.matcher(binding.emailInput.getText()).matches())
                    jsonObject.put("email", email);
                else
                    jsonObject.put("username", email);
                jsonObject.put("password", password);
                jsonObject.put("device", getResources().getString(R.string.android));
                jsonObject.put("fcm_id", Prefs.getPrefInstance().getValue(Login.this, Const.FCM_ID, ""));
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody user_logging_in = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().user_login(user_logging_in).enqueue(new Callback<LoginUserResponse>() {
                @Override
                public void onResponse(@NonNull Call<LoginUserResponse> call, @NonNull Response<LoginUserResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            Prefs.getPrefInstance().setValue(Login.this, Const.LOGIN_ACCESS, getString(R.string.logged_in));
                            if (binding.rememberMe.isChecked()) {
                                Prefs.getPrefInstance().setValue(Login.this, Const.PASSWORD, password);
                                Prefs.getPrefInstance().setValue(Login.this, Const.USER_NAME_EMAIL_CHECKED, email);
                                Prefs.getPrefInstance().setValue(Login.this, Const.KEEP_USER_LOGGED_IN, "1");
                            } else {
                                Prefs.getPrefInstance().setValue(Login.this, Const.PASSWORD, "");
                                Prefs.getPrefInstance().setValue(Login.this, Const.USER_NAME_EMAIL_CHECKED, "");
                                Prefs.getPrefInstance().setValue(Login.this, Const.KEEP_USER_LOGGED_IN, "0");
                            }
                            Prefs.getPrefInstance().setValue(Login.this, Const.EMAIL, response.body().getData().get(0).getEmail());
                            Prefs.getPrefInstance().setValue(Login.this, Const.USER_ID, response.body().getData().get(0).getUserId());
                            Prefs.getPrefInstance().setValue(Login.this, Const.USER_NAME, response.body().getData().get(0).getUsername());
                            Prefs.getPrefInstance().setValue(Login.this, Const.PHONE_NUMBER, response.body().getData().get(0).getPhone());
                            Prefs.getPrefInstance().setValue(Login.this, Const.USER_FULL_NAME, response.body().getData().get(0).getName());
                            Prefs.getPrefInstance().setValue(Login.this, Const.TOKEN, response.body().getToken());
                            if (response.body().getData().get(0).getImage() != null && !response.body().getData().get(0).getImage().isEmpty())
                                Prefs.getPrefInstance().setValue(Login.this, Const.PROFILE_IMAGE, response.body().getData().get(0).getImage());

                            binding.loader.setVisibility(View.GONE);
                            AppUtil.show_Snackbar(Login.this, binding.mainContainer, response.body().getMessage(), true);
                            startActivity(new Intent(Login.this, HomeScreen.class));
                        } else {
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        binding.loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<LoginUserResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    private void callSkipUser() {
        if (AppUtil.isInternetAvailable(Login.this)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                String device_type = "ANDROID";
                jsonObject.put("fcm_id", Prefs.getPrefInstance().getValue(Login.this, Const.FCM_ID, ""));
                jsonObject.put("device", device_type);
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody user_logging_in = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().skip_user(user_logging_in).enqueue(new Callback<SkipUserResponse>() {
                @Override
                public void onResponse(@NonNull Call<SkipUserResponse> call, @NonNull Response<SkipUserResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            Prefs.getPrefInstance().setValue(Login.this, Const.LOGIN_ACCESS, getString(R.string.home));

                            Prefs.getPrefInstance().setValue(Login.this, Const.USER_ID, response.body().getUserId());
                            Prefs.getPrefInstance().setValue(Login.this, Const.TOKEN, response.body().getToken());

                            binding.loader.setVisibility(View.GONE);
                            AppUtil.show_Snackbar(Login.this, binding.mainContainer, response.body().getMessage(), true);

                            Intent i = new Intent(Login.this, HomeScreen.class);
                            startActivity(i);
                            finish();

                        } else {
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        binding.loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<SkipUserResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }

    }

    private void setTerms_PrivacyText() {
        SpannableStringBuilder spanTxt = new SpannableStringBuilder("Please read");

        spanTxt.append(" ");
        spanTxt.append(getString(R.string.privacy_policy));

        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                startActivity(new Intent(Login.this, Policy.class).putExtra("title", getString(R.string.privacy_policy)).putExtra("type", "privacy").putExtra("access", "desc"));
            }
        }, spanTxt.length() - getString(R.string.privacy_policy).length(), spanTxt.length(), 0);
        spanTxt.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), spanTxt.length() - getString(R.string.privacy_policy).length(), spanTxt.length(), 0);

        spanTxt.append(" along with ");
        spanTxt.append(getString(R.string.terms_and_conditions));

        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                startActivity(new Intent(Login.this, Policy.class).putExtra("title", getString(R.string.terms_and_conditions)).putExtra("type", "terms").putExtra("access", "desc"));
            }
        }, spanTxt.length() - getString(R.string.terms_and_conditions).length(), spanTxt.length(), 0);
        spanTxt.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), spanTxt.length() - getString(R.string.terms_and_conditions).length(), spanTxt.length(), 0);

        binding.privacyPolicy.setMovementMethod(LinkMovementMethod.getInstance());
        binding.privacyPolicy.setText(spanTxt, TextView.BufferType.SPANNABLE);
    }
}