package com.backstage.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.backstage.R;
import com.backstage.data.models.WebView.WebViewResponse;
import com.backstage.data.remote.APIUtils;
import com.backstage.databinding.ActivityPolicyBinding;
import com.backstage.util.AppUtil;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Policy extends AppCompatActivity {

    private String type;
    private String pageTitle;
    private ActivityPolicyBinding binding;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        binding = ActivityPolicyBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        binding.loader.setVisibility(View.GONE);
        binding.noDataFound.setVisibility(View.GONE);

        type = getIntent().getStringExtra("type");
        pageTitle = getIntent().getStringExtra("title");
        binding.title.setText(pageTitle);
            show_description();
        binding.back.setOnClickListener(view1 -> onBackPressed());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    private void show_description() {
        if (AppUtil.isInternetAvailable(Policy.this)) {
            binding.loader.setVisibility(View.VISIBLE);
            APIUtils.getAPIService().call_web_view().enqueue(new Callback<WebViewResponse>() {
                @Override
                public void onResponse(@NonNull Call<WebViewResponse> call, @NonNull Response<WebViewResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (type.toLowerCase().equals("privacy".toLowerCase())) {
                                if (response.body().getPrivacyData() != null && !response.body().getPrivacyData().isEmpty()) {
                                    binding.webView.loadDataWithBaseURL(null, response.body().getPrivacyData().get(0).getDescription(), "text/html", "utf-8", null);

                                    binding.scrollView.setVisibility(View.VISIBLE);
                                    binding.loader.setVisibility(View.GONE);
                                    binding.noDataFound.setVisibility(View.GONE);
                                } else {
                                    binding.scrollView.setVisibility(View.GONE);
                                    binding.noDataFound.setVisibility(View.VISIBLE);
                                    binding.loader.setVisibility(View.GONE);
                                }
                            } else {
                                if (response.body().getTermsData() != null && !response.body().getTermsData().isEmpty()) {
                                    binding.webView.loadDataWithBaseURL(null, response.body().getTermsData().get(0).getDescription(), "text/html", "utf-8", null);
                                    binding.scrollView.setVisibility(View.VISIBLE);
                                    binding.loader.setVisibility(View.GONE);
                                    binding.noDataFound.setVisibility(View.GONE);
                                } else {
                                    binding.scrollView.setVisibility(View.GONE);
                                    binding.noDataFound.setVisibility(View.VISIBLE);
                                    binding.loader.setVisibility(View.GONE);
                                }
                            }
                        } else {
                            binding.scrollView.setVisibility(View.GONE);
                            binding.noDataFound.setVisibility(View.VISIBLE);
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(Policy.this).inflate(AppUtil.setLanguage(Policy.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(Policy.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });

                        }
                    } else {
                        binding.scrollView.setVisibility(View.GONE);
                        binding.noDataFound.setVisibility(View.VISIBLE);
                        binding.loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(Policy.this).inflate(AppUtil.setLanguage(Policy.this, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(Policy.this)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<WebViewResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.scrollView.setVisibility(View.GONE);
                    binding.noDataFound.setVisibility(View.VISIBLE);
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(Policy.this).inflate(AppUtil.setLanguage(Policy.this, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(Policy.this)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            binding.loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(Policy.this).inflate(AppUtil.setLanguage(Policy.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(Policy.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }
}