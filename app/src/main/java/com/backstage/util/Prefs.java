package com.backstage.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.backstage.data.models.video.MenuItemsData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class Prefs {

    private static final String TAG = "TAG --> " + Prefs.class.getSimpleName();

    //Single Instance object
    private static Prefs instance = null;

    private SharedPreferences sharedPreference = null;

    //Single Instance get
    public static Prefs getPrefInstance() {
        if (instance == null)
            instance = new Prefs();

        return instance;
    }

    @SuppressWarnings("static-access")
    private void openPrefs(Context context) {
        sharedPreference = context.getSharedPreferences(Const.PREFS_FILENAME, context.MODE_PRIVATE);
    }

    public void setValue(Context context, String key, String value) {
        openPrefs(context);
        SharedPreferences.Editor prefsEdit = sharedPreference.edit();
        prefsEdit.putString(key, value);
        prefsEdit.apply();
        prefsEdit = null;
        sharedPreference = null;
    }

    public String getValue(Context context, String key, String value) {
        openPrefs(context);
        String result = sharedPreference.getString(key, value);
        sharedPreference = null;
        return result;
    }

    public void setValue(Context context, String key, boolean value) {
        openPrefs(context);
        SharedPreferences.Editor prefsEdit = sharedPreference.edit();
        prefsEdit.putBoolean(key, value);
        prefsEdit.apply();
        prefsEdit = null;
        sharedPreference = null;
    }

    public boolean getValue(Context context, String key, boolean value) {
        openPrefs(context);
        boolean result = sharedPreference.getBoolean(key, value);
        sharedPreference = null;
        return result;
    }

    public void setValue(Context context, String key, int value) {
        openPrefs(context);
        SharedPreferences.Editor prefsEdit = sharedPreference.edit();
        prefsEdit.putInt(key, value);
        prefsEdit.apply();
        prefsEdit = null;
        sharedPreference = null;
    }

    public int getValue(Context context, String key, int value) {
        openPrefs(context);
        int result = sharedPreference.getInt(key, value);
        sharedPreference = null;
        return result;
    }

    public void saveListInLocal(Context context,ArrayList<MenuItemsData> list, String key) {
        openPrefs(context);
        SharedPreferences.Editor editor = sharedPreference.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }

    public static void saveSharedPreferencesLogList(Context context, ArrayList<MenuItemsData> collageList) {
        SharedPreferences mPrefs = context.getSharedPreferences("PhotoCollage", context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(collageList);
        prefsEditor.putString("myJson", json);
        prefsEditor.commit();
    }

    public static ArrayList<MenuItemsData> loadSharedPreferencesLogList(Context context) {
        ArrayList<MenuItemsData> savedCollage = new ArrayList<MenuItemsData>();
        SharedPreferences mPrefs = context.getSharedPreferences("PhotoCollage", context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("myJson", "");
        if (json.isEmpty()) {
            savedCollage = new ArrayList<MenuItemsData>();
        } else {
            Type type = new TypeToken<ArrayList<MenuItemsData>>() {
            }.getType();
            savedCollage = gson.fromJson(json, type);
        }

        return savedCollage;
    }

    public ArrayList<MenuItemsData> getListFromLocal(Context context,String key) {
        openPrefs(context);
        Gson gson = new Gson();
        String json = sharedPreference.getString(key, null);
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public void remove(Context context, String key) {
        openPrefs(context);
        SharedPreferences.Editor prefsEditor = sharedPreference.edit();
//        prefsEditor.remove(key).commit();
        prefsEditor.remove(key).apply();
        prefsEditor = null;
        sharedPreference = null;
    }
}
