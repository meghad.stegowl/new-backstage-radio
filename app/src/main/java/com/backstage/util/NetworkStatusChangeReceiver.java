package com.backstage.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

//import com.flow_activo.playerManager.MediaController;

public class NetworkStatusChangeReceiver extends BroadcastReceiver {

    AppManageInterface appManageInterface;

    public NetworkStatusChangeReceiver(AppManageInterface appManageInterface) {
        this.appManageInterface = appManageInterface;
    }

    public NetworkStatusChangeReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() != null && (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE") || intent.getAction().equals("android.net.wifi.WIFI_STATE_CHANGED"))) {
            boolean status = AppUtil.isInternetAvailable(context);
        }
    }
}
