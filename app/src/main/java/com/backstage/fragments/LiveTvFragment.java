package com.backstage.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.backstage.application.Backstage;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.backstage.R;
import com.backstage.Services.FloatingWidgetService;
import com.backstage.activities.HomeScreen;
import com.backstage.activities.Login;
import com.backstage.adapters.CarousalAdapter;
import com.backstage.data.models.common.ChatModal;
import com.backstage.data.models.live_tv.LikeUnlikeResponse;
import com.backstage.data.models.live_tv.LiveTvResponse;
import com.backstage.data.models.video.MenuItemsResponse;
import com.backstage.data.remote.APIUtils;
import com.backstage.databinding.ActivityLiveTvBinding;
import com.backstage.util.AppManageInterface;
import com.backstage.util.AppUtil;
import com.backstage.util.Const;
import com.backstage.util.Prefs;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackPreparer;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_OUT_OF_MEMORY;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_REMOTE;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_RENDERER;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_SOURCE;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_UNEXPECTED;

public class LiveTvFragment extends Fragment implements PlayerControlView.VisibilityListener, PlaybackPreparer {

    private static LiveTvFragment instance = null;
    ActivityLiveTvBinding binding;

    private AppManageInterface appManageInterface;
    private String path, type, id, category_id;
    private Context context;
    private FragmentManager fragmentManager;
    private Tracker mTracker;
    public static SimpleExoPlayer liveTvexoPlayer;
    private String playBackUrl;
    private Handler handler;
    private String video_id;
    private FirebaseRecyclerAdapter adapter;
    private CarousalAdapter carousalAdapter;
    private Runnable runnable;
    private String shareDetails;
    private ImageView imgViewChangeScreenOrientation;
    public static boolean isfromLiveTvFragment = true;
    public static boolean isLiveTVShareClicked = false;

    public static synchronized LiveTvFragment getInstance() {
        return instance;
    }

    public static synchronized LiveTvFragment newInstance() {
        return instance = new LiveTvFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mTracker = Backstage.getDefaultTracker();
        binding = ActivityLiveTvBinding.inflate(inflater, container, false);
        isfromLiveTvFragment = true;
        HomeScreen.isPipModeEnabled = false;
        VideoFragment.isFromVideoFragment = false;
        HomeScreen.islivetvBackClicked = true;
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Screen - " + "Live Tv");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        binding.playerView.onResume();
        if (liveTvexoPlayer != null)
            liveTvexoPlayer.setPlayWhenReady(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        binding.playerView.onResume();
        if (liveTvexoPlayer != null)
            liveTvexoPlayer.setPlayWhenReady(true);
    }

    @Override
    public void onPause() {
        binding.playerView.onPause();
        if (liveTvexoPlayer != null) {
            liveTvexoPlayer.setPlayWhenReady(false);
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        binding.playerView.onPause();
        if (liveTvexoPlayer != null)
            liveTvexoPlayer.setPlayWhenReady(false);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        binding.playerView.onPause();
        if (liveTvexoPlayer != null) {
            liveTvexoPlayer.release();
            liveTvexoPlayer = null;
        }
        super.onDestroy();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        binding.noDataFound.setVisibility(View.GONE);
        binding.loader.setVisibility(View.GONE);
        HomeScreen.isVideoOpen = true;
        appManageInterface.miniPlayerVisibilityGone(true);
        appManageInterface.bottomanduppermanage(true);
        appManageInterface.hideBothAerrow();
        init();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Activity activity = (HomeScreen) context;
        try {
            appManageInterface = (AppManageInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement Interface");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public  static void closeLoveTvPlayer(){
        if (liveTvexoPlayer != null) {
            liveTvexoPlayer.setPlayWhenReady(false);
            liveTvexoPlayer.release();
            liveTvexoPlayer = null;
        }
    }

    private void initializePlayer() {
        FloatingWidgetService.hidePipPlayer();

        binding.videoOverlay.setVisibility(View.GONE);
        liveTvexoPlayer = new SimpleExoPlayer.Builder(context).build();
        binding.playerView.setPlayer(liveTvexoPlayer);
        binding.playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
        binding.playerView.setUseController(true);

        liveTvexoPlayer.setPlayWhenReady(true);
        liveTvexoPlayer.seekTo(0);
        appManageInterface.setLiveTvVideo(playBackUrl, "getReady", 0);
        Prefs.getPrefInstance().setValue(context, Const.VIDEO_URL, playBackUrl);

        imgViewChangeScreenOrientation = binding.playerView.findViewById(R.id.exo_fullscreen_icon);
        imgViewChangeScreenOrientation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liveTvexoPlayer.setPlayWhenReady(false);
                liveTvexoPlayer.release();
                Intent serviceIntent = new Intent(context, FloatingWidgetService.class);
                serviceIntent.putExtra("videoUri", playBackUrl);
                serviceIntent.putExtra("isfrom", "livetv");
//                serviceIntent.putExtra("videoUri", "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/SubaruOutbackOnStreetAndDirt.mp4");
                getActivity().startService(serviceIntent);
            }
        });

        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setUsage(C.USAGE_MEDIA)
                .setContentType(C.CONTENT_TYPE_MOVIE)
                .build();
        if (liveTvexoPlayer.getAudioComponent() != null)
            liveTvexoPlayer.getAudioComponent().setAudioAttributes(audioAttributes, true);
        try {
            if (!playBackUrl.equals("")) {
                DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context, Util.getUserAgent(context, getResources().getString(R.string.app_name)));
                MediaSource videoSource = new HlsMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(playBackUrl));
                liveTvexoPlayer.prepare(videoSource, true, true);
            }
        } catch (Exception e) {

        }
        liveTvexoPlayer.addListener(new Player.EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                switch (error.type) {
                    case TYPE_SOURCE:
                        binding.videoOverlay.setVisibility(View.VISIBLE);
                        break;
                    case TYPE_RENDERER:
                    case TYPE_UNEXPECTED:
                        liveTvexoPlayer.retry();
                        break;
                    case TYPE_OUT_OF_MEMORY:
                    case TYPE_REMOTE:
                        AppUtil.show_Snackbar(context, binding.playerView, error.getMessage(), true);
                        break;
                }
            }
        });
    }

    @Override
    public void preparePlayback() {

    }

    @Override
    public void onVisibilityChange(int visibility) {

    }

    private void init() {
        binding.noDataFound.setVisibility(View.GONE);
        binding.loader.setVisibility(View.GONE);
        binding.videoOverlay.setVisibility(View.VISIBLE);
        getCarousal();
        getMessages();

        if (getArguments() != null) {
            path = Prefs.getPrefInstance().getValue(context, Const.MENU_LIVE_TV_ID, "");
            type = "Videos";
        }

        if (path != null) {
            call_video();
        } else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(d -> {
                dialog.dismiss();
                getActivity().finish();
            });
        }

        binding.fav.setOnClickListener(view -> {
            calLikeUnlike();
        });
        binding.share.setOnClickListener(view13 -> {
            isLiveTVShareClicked = true;
            HomeScreen.isPipModeEnabled = true;
            onResume();
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, shareDetails);
            sendIntent.setType("text/plain");
            Intent shareIntent = Intent.createChooser(sendIntent, null);
            isLiveTVShareClicked = false;
            HomeScreen.isPipModeEnabled = true;
            startActivity(shareIntent);
            onResume();
        });
        binding.back.setOnClickListener(v -> {
//            appManageInterface.showMainScreen();
            appManageInterface.livetvBack();
//            appManageInterface.go_back();
        });
        binding.sendMessage.setOnClickListener(view -> {
            String msg = binding.messageInput.getText().toString().trim();
            if (!TextUtils.isEmpty(msg)) {
                if (AppUtil.isInternetAvailable(context)) {
                    String user_id = "";
                    String name;
                    String path = "User";
                    String profile_pic = "";
                    String type = "user";
                    if (Prefs.getPrefInstance().getValue(context, Const.USER_NAME, "") != null && !Prefs.getPrefInstance().getValue(context, Const.USER_NAME, "").isEmpty())
                        name = Prefs.getPrefInstance().getValue(context, Const.USER_NAME, "");
                    else name = "User";

                    if (Prefs.getPrefInstance().getValue(context, Const.PROFILE_IMAGE, "") != null && !Prefs.getPrefInstance().getValue(context, Const.PROFILE_IMAGE, "").isEmpty())
                        profile_pic = Prefs.getPrefInstance().getValue(context, Const.PROFILE_IMAGE, "");
                    else profile_pic = "http://34.205.55.93/admin/assets/images/avtar.png";

                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    DatabaseReference myRef = database.getReference();
                    DatabaseReference databaseReference = myRef.child(path).push();
                    Map<String, Object> map = new HashMap<>();
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm a");
                    sdf.setTimeZone(TimeZone.getTimeZone("America/New_York"));
                    sdfTime.setTimeZone(TimeZone.getTimeZone("America/New_York"));
                    long time = System.currentTimeMillis();
                    String date = sdf.format(time);
                    String currentTime = sdfTime.format(time);
                    map.put("comment", binding.messageInput.getText().toString());
                    map.put("commentByName", name);
                    map.put("commentDate", date);
                    map.put("commentImageURL", profile_pic);
                    map.put("time", currentTime);
                    map.put("type", type);
                    databaseReference.setValue(map);
                    binding.messageInput.getText().clear();
                } else {
                    Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Please Enter Some Text", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getMessages() {
        binding.messageInput.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                ((InputMethodManager) Objects.requireNonNull(context.getSystemService(getActivity().INPUT_METHOD_SERVICE))).hideSoftInputFromWindow(v.getWindowToken(), 0);
                binding.sendMessage.performClick();
                return true;
            }
            return false;
        });
        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference();
        String path = "User";

        Query query = myRef.child(path);

        FirebaseRecyclerOptions<ChatModal> options = new FirebaseRecyclerOptions.Builder<ChatModal>()
                .setQuery(query, snapshot -> {
                    String time = "", date = "", image = "", name = "", comment = "", type = "";
                    if (snapshot.hasChild("time") && snapshot.child("time").getValue() != null)
                        time = snapshot.child("time").getValue().toString();
                    if (snapshot.hasChild("commentDate") && snapshot.child("commentDate").getValue() != null)
                        date = snapshot.child("commentDate").getValue().toString();
                    if (snapshot.hasChild("commentImageURL") && snapshot.child("commentImageURL").getValue() != null)
                        image = snapshot.child("commentImageURL").getValue().toString();
                    if (snapshot.hasChild("commentByName") && snapshot.child("commentByName").getValue() != null)
                        name = snapshot.child("commentByName").getValue().toString();
                    if (snapshot.hasChild("comment") && snapshot.child("comment").getValue() != null)
                        comment = snapshot.child("comment").getValue().toString();
                    if (snapshot.hasChild("type") && snapshot.child("type").getValue() != null)
                        type = snapshot.child("type").getValue().toString();
                    return new ChatModal(time, date, image, name, comment, type);
                })
                .build();

        adapter = new FirebaseRecyclerAdapter<ChatModal, ViewHolder>(options) {
            @NotNull
            @Override
            public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_user_comment, parent, false));
            }

            @Override
            protected void onBindViewHolder(@NotNull ViewHolder holder, final int position, @NotNull ChatModal model) {
                if (model.getType().equals("user")) {
                    holder.adminItemContainer.setVisibility(View.GONE);
                    holder.adminView.setVisibility(View.GONE);
                    holder.userItemContainer.setVisibility(View.VISIBLE);
                    holder.userView.setVisibility(View.VISIBLE);
                    holder.userComment.setText(model.getComment());
                    holder.userName.setText(model.getCommentByName());
                    holder.userCommentTime.setText(model.getTime());
                    Glide
                            .with(Backstage.applicationContext)
                            .load(model.getImage())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .thumbnail(0.25f)
                            .centerCrop()
                            .error(R.drawable.ic_profile_icon)
                            .placeholder(R.drawable.ic_profile_icon)
                            .into(holder.userImage);
                } else {
                    holder.adminItemContainer.setVisibility(View.VISIBLE);
                    holder.adminView.setVisibility(View.VISIBLE);
                    holder.userItemContainer.setVisibility(View.GONE);
                    holder.userView.setVisibility(View.GONE);
                    holder.adminComment.setText(model.getComment());
                    holder.adminCommentTime.setText(model.getTime());
                }


            }
        };
        binding.listing.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        binding.listing.setLayoutManager(linearLayoutManager);
        binding.listing.setAdapter(adapter);
        adapter.startListening();
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                binding.listing.postDelayed(() -> binding.listing.scrollToPosition(adapter.getItemCount() - 1), 1000);
            }

            @Override
            public void onCancelled(@NotNull DatabaseError error) {
                // Failed to read value
            }
        });
    }

    public boolean getVideoIsPlaying() {
        return liveTvexoPlayer != null && (liveTvexoPlayer.isPlaying() || !liveTvexoPlayer.isPlaying());
    }

    public long getCurrentPosition() {
        return liveTvexoPlayer.getCurrentPosition();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout userItemContainer;
        LinearLayout adminItemContainer;
        ImageView userImage;
        TextView userName;
        TextView userComment;
        TextView userCommentTime;
        TextView adminCommentTime;
        TextView adminComment;
        View adminView;
        View userView;


        ViewHolder(@NonNull View itemView) {
            super(itemView);
            userItemContainer = itemView.findViewById(R.id.user_item_container);
            adminItemContainer = itemView.findViewById(R.id.admin_item_container);
            userImage = itemView.findViewById(R.id.user_image);
            userName = itemView.findViewById(R.id.user_name);
            userComment = itemView.findViewById(R.id.user_comment);
            userCommentTime = itemView.findViewById(R.id.user_comment_time);
            adminCommentTime = itemView.findViewById(R.id.admin_comment_time);
            adminComment = itemView.findViewById(R.id.admin_comment);
            adminView = itemView.findViewById(R.id.admin_view);
            userView = itemView.findViewById(R.id.user_view);
        }
    }

    private void call_video() {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody call_menu_items = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().call_menu_items(call_menu_items, path, type).enqueue(new Callback<MenuItemsResponse>() {
                @Override
                public void onResponse(@NonNull Call<MenuItemsResponse> call, @NonNull Response<MenuItemsResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                playBackUrl = response.body().getData().get(0).getVideosLink();
//                                playBackUrl = "https:\\/\\/www.learningcontainer.com\\/wp-content\\/uploads\\/2020\\/05\\/sample-mp4-file.mp4";
                                shareDetails = "Video Url: " + playBackUrl + "\n" + "App Name: " + context.getResources().getString(R.string.app_name);
                                video_id = response.body().getData().get(0).getVideosId();
                                binding.likeCount.setText(response.body().getData().get(0).getVideoLikeCount());
                                binding.fav.setSelected(response.body().getData().get(0).getVideoLikeStatus());
                                initializePlayer();
                                binding.loader.setVisibility(View.GONE);
                                binding.noDataFound.setVisibility(View.GONE);
                            } else {
                                binding.noDataFound.setVisibility(View.GONE);
                                binding.loader.setVisibility(View.GONE);
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            binding.noDataFound.setVisibility(View.GONE);
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MenuItemsResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.noDataFound.setVisibility(View.GONE);
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            binding.loader.setVisibility(View.GONE);
            binding.noDataFound.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    private void getCarousal() {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_live_tv_slider = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().get_live_tv_slider(get_live_tv_slider).enqueue(new Callback<LiveTvResponse>() {
                @Override
                public void onResponse(@NonNull Call<LiveTvResponse> call, @NonNull Response<LiveTvResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                carousalAdapter = new CarousalAdapter(context, response.body().getData());
                                binding.carousal.setAdapter(carousalAdapter);
                                binding.carousal.setOffscreenPageLimit(response.body().getData().size());
                                binding.carousal.setCurrentItem(0, true);
                                binding.carousal.getChildAt(0).setOverScrollMode(View.OVER_SCROLL_NEVER);

                                final int speedScroll = 5000;
                                handler = new Handler();
                                runnable = new Runnable() {
                                    int count = 0;
                                    boolean flag = true;

                                    @Override
                                    public void run() {
                                        if (count < carousalAdapter.getItemCount()) {
                                            if (count == carousalAdapter.getItemCount() - 1) {
                                                flag = false;
                                            } else if (count == 0) {
                                                flag = true;
                                            }
                                            if (flag) count++;
                                            else count--;
                                            binding.carousal.setCurrentItem(count, true);
                                            handler.postDelayed(this, speedScroll);
                                        }
                                    }
                                };
                                handler.postDelayed(runnable, speedScroll);
                                binding.carousal.setVisibility(View.VISIBLE);
                            } else {
                                binding.carousal.setVisibility(View.GONE);
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            getActivity().finish();
                        }
                    } else {
                        binding.carousal.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<LiveTvResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.carousal.setVisibility(View.GONE);
                }
            });
        } else {
            binding.carousal.setVisibility(View.GONE);
            binding.loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.no_internet_connection));
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setVisibility(View.GONE);

            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                dialog.dismiss();
            });

        }
    }

    private void calLikeUnlike() {
        if (AppUtil.isInternetAvailable(context)) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("videos_id", video_id);
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody share_my_app = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().call_like_unlike(share_my_app).enqueue(new Callback<LikeUnlikeResponse>() {
                @Override
                public void onResponse(@NonNull Call<LikeUnlikeResponse> call, @NonNull Response<LikeUnlikeResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            binding.likeCount.setText(response.body().getVideosLikeCount());
                            binding.fav.setSelected(response.body().getVideosLikeStatus());
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<LikeUnlikeResponse> call, @NonNull Throwable t) {
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

}
