package com.backstage.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.backstage.R;
import com.backstage.activities.HomeScreen;
import com.backstage.activities.LiveTv;
import com.backstage.activities.Login;
import com.backstage.data.models.common.CommonResponse;
import com.backstage.data.models.user.SkipUserResponse;
import com.backstage.data.models.user.UploadImageResponse;
import com.backstage.data.remote.APIUtils;
import com.backstage.databinding.FragmentSignUpBinding;
import com.backstage.util.AppUtil;
import com.backstage.util.Const;
import com.backstage.util.Prefs;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static com.github.florent37.runtimepermission.RuntimePermission.askPermission;

public class SignUpFragment extends Fragment{

    FragmentSignUpBinding binding;
    private static SignUpFragment instance = null;
    private static String uploaded_image = "";
    Handler handler;
    int rollId = 1;
    private String path;
    private String type;
    private String pageTitle;
    private Context context;
    private FragmentManager fragmentManager;
    private Activity activity;
    private Uri mCropImageUri;
    private String image = "";

    private String showpicturedialog_title, selectfromgallerymsg, selectfromcameramsg, canceldialog;
    private final int GALLERY = 2;
    private final int CAMERA = 1;
    private Uri resultUri;
    private Bitmap bitmap;

    public static synchronized SignUpFragment getInstance() {
        return instance;
    }

    public static synchronized SignUpFragment newInstance() {
        return instance = new SignUpFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentSignUpBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        binding.loader.setVisibility(View.GONE);
        activity = getActivity();
        if (getArguments() != null) {
            path = getArguments().getString("path");
            type = getArguments().getString("type");
            pageTitle = getArguments().getString("title");
        }
        init();
    }

    private void init() {
        binding.loader.setVisibility(View.GONE);
        handler = new Handler();
        rollId = 1;
        binding.scrollView.scrollTo(0, 0);

        if (!Prefs.getPrefInstance().getValue(context, Const.MENU_LIVE_TV_STATUS, "").isEmpty()
                && Prefs.getPrefInstance().getValue(context, Const.MENU_LIVE_TV_STATUS, "") != null
                && Prefs.getPrefInstance().getValue(context, Const.MENU_LIVE_TV_STATUS, "".toLowerCase()).equals("visible"))
            binding.liveTv.setVisibility(View.VISIBLE);
        else binding.liveTv.setVisibility(View.GONE);

        binding.home.setOnClickListener(view -> {
            callSkipUser();
        });
        binding.liveTv.setOnClickListener(view -> startActivity(new Intent(context, LiveTv.class).putExtra("type", "Videos").putExtra("path", Prefs.getPrefInstance().getValue(context, Const.MENU_LIVE_TV_ID, ""))
                .putExtra("title", context.getResources().getString(R.string.live_tv))));


        binding.usernameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.usernameContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.nameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.nameContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.passwordInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.passwordContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.confirmPasswordInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.confirmPasswordContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.phoneInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.phoneContainer.setError("");
                if (s.length() > 1 && !s.toString().substring(s.length() - 1).equals("-") && (s.length() == 3 || s.length() == 7) && binding.phoneInput.getText() != null)
                    binding.phoneInput.getText().append("-");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.emailInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.emailContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, Login.class);
                startActivity(i);
                getActivity().finish();
            }
        });

        binding.buttonRegister.setOnClickListener(view -> {
            if (binding.nameInput.getText() == null) {
                binding.nameContainer.setError(getResources().getString(R.string.name_empty));
            } else if (binding.nameInput.getText().toString().trim().length() == 0) {
                binding.nameContainer.setError(getResources().getString(R.string.name_empty));
            } else if (binding.usernameInput.getText() == null) {
                binding.usernameContainer.setError(getResources().getString(R.string.username_empty));
            } else if (binding.usernameInput.getText().toString().trim().length() == 0) {
                binding.usernameContainer.setError(getResources().getString(R.string.username_empty));
            } else if (binding.emailInput.getText() == null) {
                binding.emailContainer.setError(getResources().getString(R.string.email_username_empty));
            } else if (binding.emailInput.getText().toString().trim().length() == 0) {
                binding.emailContainer.setError(getResources().getString(R.string.email_username_empty));
            } else if (!TextUtils.isDigitsOnly(binding.emailInput.getText()) && !Patterns.EMAIL_ADDRESS.matcher(binding.emailInput.getText()).matches()) {
                binding.emailContainer.setError(getResources().getString(R.string.email_not_valid));
            } else if (binding.passwordInput.getText() == null) {
                binding.passwordContainer.setError(getResources().getString(R.string.password_empty));
            } else if (binding.passwordInput.getText().toString().trim().length() == 0) {
                binding.passwordContainer.setError(getResources().getString(R.string.password_empty));
            } else if (binding.confirmPasswordInput.getText() == null) {
                binding.confirmPasswordContainer.setError(getResources().getString(R.string.confirm_password_empty));
            } else if (binding.confirmPasswordInput.getText().toString().trim().length() == 0) {
                binding.confirmPasswordContainer.setError(getResources().getString(R.string.confirm_password_empty));
            } else if (!binding.confirmPasswordInput.getText().toString().equals(binding.passwordInput.getText().toString())) {
                binding.confirmPasswordContainer.setError(getResources().getString(R.string.confirm_password_not_valid));
            } else if (binding.phoneInput.getText() == null) {
                binding.phoneContainer.setError(getResources().getString(R.string.phone_number_empty));
            } else if (binding.phoneInput.getText().toString().trim().length() == 0) {
                binding.phoneContainer.setError(getResources().getString(R.string.phone_number_empty));
            } else if (binding.phoneInput.getText().toString().trim().length() < 12) {
                binding.phoneContainer.setError(getResources().getString(R.string.phone_number_incorrect));
            } else {
                binding.usernameInput.clearFocus();
                binding.nameInput.clearFocus();
                binding.emailInput.clearFocus();
                binding.passwordInput.clearFocus();
                binding.confirmPasswordInput.clearFocus();
                String image = Prefs.getPrefInstance().getValue(context, Const.SET_PROFILE_IMAGE, "");
//                if (image.equals("")) {
                if (resultUri == null) {
                    callRegister(binding.nameInput.getText().toString(), binding.usernameInput.getText().toString(), binding.emailInput.getText().toString(), binding.passwordInput.getText().toString(),
                            binding.confirmPasswordInput.getText().toString(), binding.phoneInput.getText().toString(), image);
                } else {
                    uploadProfilePicture(/*image,*/ binding.nameInput.getText().toString(), binding.usernameInput.getText().toString(), binding.emailInput.getText().toString(), binding.passwordInput.getText().toString(),
                            binding.confirmPasswordInput.getText().toString(), binding.phoneInput.getText().toString(),
                            new File(getRealPathFromURIPath(resultUri, getActivity())), resultUri);
                }

            }
        });

        binding.selectPicture.setOnClickListener(view -> {
            binding.nameInput.clearFocus();
            binding.usernameInput.clearFocus();
            binding.passwordInput.clearFocus();
            binding.phoneInput.clearFocus();
            binding.confirmPasswordInput.clearFocus();

            requestMultiplePermissions();
            showpicturedialog_title = "Select the Action";
            selectfromgallerymsg = "Select photo from Gallery";
            selectfromcameramsg = "Capture photo from Camera";
            canceldialog = "Cancel";
            showPictureDialog_chooser();
        });

    }

    private void callRegister(String name, String username, String email, String password, String confirm_password, String phone, String url) {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                String device_type = "ANDROID";
                jsonObject.put("name", name);
                jsonObject.put("username", username);
                jsonObject.put("email", email);
                jsonObject.put("phone", phone);
                jsonObject.put("password", password);
                jsonObject.put("confirm_password", confirm_password);
                jsonObject.put("fcm_id", Prefs.getPrefInstance().getValue(context, Const.FCM_ID, ""));
                jsonObject.put("device", device_type);
//                jsonObject.put("image", uploaded_image);
                jsonObject.put("image", url);
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody user_registering = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().user_registering(user_registering).enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(response.body().getMessage()));

                            (dialog_view.findViewById(R.id.dialog_cancel)).setVisibility(View.GONE);
                            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                dialog.dismiss();
                                Intent i = new Intent(context, Login.class);
                                startActivity(i);
                                getActivity().finish();
                            });
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);


                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(response.body().getMessage()));

                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        binding.loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    private void callSkipUser() {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                String device_type = "ANDROID";
                jsonObject.put("fcm_id", Prefs.getPrefInstance().getValue(context, Const.FCM_ID, ""));
                jsonObject.put("device", device_type);
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody user_logging_in = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().skip_user(user_logging_in).enqueue(new Callback<SkipUserResponse>() {
                @Override
                public void onResponse(@NonNull Call<SkipUserResponse> call, @NonNull Response<SkipUserResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            Prefs.getPrefInstance().setValue(context, Const.LOGIN_ACCESS, getString(R.string.home));

                            Prefs.getPrefInstance().setValue(context, Const.USER_ID, response.body().getUserId());
                            Prefs.getPrefInstance().setValue(context, Const.TOKEN, response.body().getToken());

                            binding.loader.setVisibility(View.GONE);
                            AppUtil.show_Snackbar(context, binding.mainContainer, response.body().getMessage(), true);
                            Intent i = new Intent(context, HomeScreen.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        binding.loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<SkipUserResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }

    }

    private void requestMultiplePermissions() {
        Dexter.withActivity((Activity) context)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
//                            Toast.makeText(context, "All permissions are granted by user!", Toast.LENGTH_SHORT).show();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            //openSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(context, "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showPictureDialog_chooser() {
        android.app.AlertDialog.Builder pictureDialog = new android.app.AlertDialog.Builder(context);
        pictureDialog.setTitle(showpicturedialog_title);
        String[] pictureDialogItems = {selectfromgallerymsg, selectfromcameramsg, canceldialog};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                            case 3:
                                dialog.dismiss();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        startActivityForResult(intent, CAMERA);
    }

    private void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }

        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    CropImage(contentURI);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } else if (requestCode == CAMERA) {
            File f = new File(Environment.getExternalStorageDirectory().toString());
            for (File temp : f.listFiles()) {
                if (temp.getName().equals("temp.jpg")) {
                    f = temp;
                    Log.d("mytag", "f=temp");
                    break;
                }
            }

            try {
                CropImage(Uri.fromFile(f));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == UCrop.REQUEST_CROP) {
            handleUCropResult(data);
        }
    }

    private void CropImage(Uri contentURI) throws IOException {
        Uri destinationUri = Uri.fromFile(new File(context.getCacheDir(), "temp.jpg"));
        UCrop.Options options = new UCrop.Options();
//      options.setCompressionQuality(IMAGE_COMPRESSION);
        options.setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary));
        options.setStatusBarColor(ContextCompat.getColor(context, R.color.colorPrimary));
        options.setActiveWidgetColor(ContextCompat.getColor(context, R.color.colorPrimary));

        UCrop.of(contentURI, destinationUri)
                .withOptions(options)
                .start(context, SignUpFragment.this);
    }

    private void handleUCropResult(Intent data) {
        if (data == null) {
            setResultCancelled();
            return;
        }
        try {
            resultUri = UCrop.getOutput(data);
            bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), resultUri);
            Log.d("mytag", "my bitmap iss-" + bitmap);
//             Uploadimagetoserver(new File(getRealPathFromURIPath(resultUri, getActivity())), resultUri);
            binding.userProfile.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private void setResultCancelled() {
        Intent intent = new Intent();
        setResult(getActivity().RESULT_CANCELED, intent);
        getActivity().finish();
    }

    private void setResult(int resultCanceled, Intent intent) {

    }

    private void uploadProfilePicture(/*String filePath,*/ String name, String username, String email, String password,
                                                           String confirm_password, String phone, File file,
                                                           final Uri resultUri) {
//        File imageFile = new File(filePath);
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            final RequestBody profile_image = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), profile_image);
            Log.d("mytag", "uploadProfilePictureuploadProfilePicture is called");
//            final RequestBody profile_image = RequestBody.create(imageFile, MediaType.parse("image/*"));
//            MultipartBody.Part profile_image_body = MultipartBody.Part.createFormData("image", imageFile.getName(), profile_image);
            APIUtils.getAPIService().upload_image(fileToUpload).enqueue(new Callback<UploadImageResponse>() {
                @Override
                public void onResponse(@NonNull Call<UploadImageResponse> call, @NonNull Response<UploadImageResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            try {
                                binding.userProfile.setImageBitmap(MediaStore.Images.Media.getBitmap(context.getContentResolver(),
                                        resultUri));
                                Prefs.getPrefInstance().setValue(context, Const.SET_PROFILE_IMAGE, response.body().getImage());
                                callRegister(name, username, email, password, confirm_password, phone, response.body().getImage());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            binding.loader.setVisibility(View.GONE);
                        } else {
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(response.body().getMessage()));
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                        }
                    } else {
                        binding.loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<UploadImageResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                }
            });
        } else {
            binding.loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
        }
    }


}
