package com.backstage.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.backstage.R;
import com.backstage.Services.FloatingWidgetService;
import com.backstage.activities.HomeScreen;
import com.backstage.activities.Login;
import com.backstage.adapters.AdapterMainviewPager;
import com.backstage.application.Backstage;
import com.backstage.data.models.BannerSlider;
import com.backstage.data.models.BannerSliderData;
import com.backstage.data.models.common.CommonResponse;
import com.backstage.data.models.social_media.SocialMediaResponse;
import com.backstage.data.remote.APIUtils;
import com.backstage.databinding.FragmentSocialMediaBinding;
import com.backstage.util.AppManageInterface;
import com.backstage.util.AppUtil;
import com.backstage.util.Const;
import com.backstage.util.Prefs;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SocialMediaFragment extends Fragment {

    FragmentSocialMediaBinding binding;
    private static SocialMediaFragment instance = null;

    private AppManageInterface appManageInterface;
    private String instagramPath, fbPath, twitterPath, otherPath, pageTitle;
    private Context context = getContext();
    private FragmentManager fragmentManager;
    private boolean loadingInProgress;
    private Activity activity;
    private Tracker mTracker;

    private ArrayList<BannerSliderData> bannerSliderData = new ArrayList<>();
    AdapterMainviewPager adapterMainviewPager;
    private final Object progressTimerSync = new Object();
    private final Object sync = new Object();
    private Timer progressTimer = null;

    public static synchronized SocialMediaFragment getInstance() {
        return instance;
    }

    public static synchronized SocialMediaFragment newInstance() {
        return instance = new SocialMediaFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mTracker = Backstage.getDefaultTracker();
        binding = FragmentSocialMediaBinding.inflate(inflater, container, false);
        FloatingWidgetService.hidePipPlayer();
        HomeScreen.isPipModeEnabled = true;
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Screen - " + "Social Media");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        binding.loader.setVisibility(View.GONE);
        binding.noDataFound.setVisibility(View.GONE);
        appManageInterface.closeVideoBottom();
        activity = getActivity();
        if (getArguments() != null) {
            pageTitle = getArguments().getString("title");
        }
        binding.tvTitle.setText("Social And Booking");
        binding.drawerPopup.setSelected(true);
        getSliderData();
        call_social_media();
        init();
    }

    private void init() {

        binding.instagramContainer.setOnClickListener(view -> {
            appManageInterface.showFragment();
            WebViewFragment fragment = WebViewFragment.newInstance();
            Bundle bundle = new Bundle();
            bundle.putString("path", instagramPath);
            bundle.putString("type", "Social Media");
            bundle.putString("title", "Instagram");
            fragment.setArguments(bundle);
            AppUtil.setup_Fragment_with_bundle(fragmentManager, fragment, "Inner", "Inner", true);
        });
        binding.facebookContainer.setOnClickListener(view -> {
            appManageInterface.showFragment();
            WebViewFragment fragment = WebViewFragment.newInstance();
            Bundle bundle = new Bundle();
            bundle.putString("path", fbPath);
            bundle.putString("type", "Social Media");
            bundle.putString("title", "Facebook");
            fragment.setArguments(bundle);
            AppUtil.setup_Fragment_with_bundle(fragmentManager, fragment, "Inner", "Inner", true);

        });
        binding.twitterContainer.setOnClickListener(view -> {
            appManageInterface.showFragment();
            WebViewFragment fragment = WebViewFragment.newInstance();
            Bundle bundle = new Bundle();
            bundle.putString("path", twitterPath);
            bundle.putString("type", "Social Media");
            bundle.putString("title", "Twitter");
            fragment.setArguments(bundle);
            AppUtil.setup_Fragment_with_bundle(fragmentManager, fragment, "Inner", "Inner", true);

        });
        binding.otherContainer.setOnClickListener(view -> {

            appManageInterface.showFragment();
            WebViewFragment fragment = WebViewFragment.newInstance();
            Bundle bundle = new Bundle();
            bundle.putString("path", otherPath);
            bundle.putString("type", "Social Media");
            bundle.putString("title", "Other");
            fragment.setArguments(bundle);
            AppUtil.setup_Fragment_with_bundle(fragmentManager, fragment, "Inner", "Inner", true);
        });
        binding.back.setOnClickListener(v -> appManageInterface.go_back());

        binding.home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, HomeScreen.class);
                startActivity(i);
            }
        });
        binding.liveTvPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, LiveTvFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appManageInterface.setVisibleStatus(false, false, false, false, false);
                AppUtil.setup_Fragment(fragmentManager, RadioFragment.newInstance(), "", getResources().getString(R.string.radio), getResources().getString(R.string.live_radio), "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.ivVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, VideoFragment.newInstance(), "", "VideosCategory", "VideosCategory", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.ivFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, PlaylistFavListingFragment.newInstance(), "", "FavouritesSongsList", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.ivPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, PlaylistFavListingFragment.newInstance(), "", "PlaylistList", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.drawerPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, MenuFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });

        binding.submit.setOnClickListener(view -> {
            if (binding.nameInput.getText() == null) {
                binding.nameContainer.setError(getResources().getString(R.string.name_empty));
            } else if (binding.nameInput.getText().toString().trim().length() == 0) {
                binding.nameContainer.setError(getResources().getString(R.string.name_empty));
            } else if (binding.emailInput.getText() == null) {
                binding.emailContainer.setError(getResources().getString(R.string.email_empty));
            } else if (binding.emailInput.getText().toString().trim().length() == 0) {
                binding.emailContainer.setError(getResources().getString(R.string.email_empty));
            } else if (!TextUtils.isDigitsOnly(binding.emailInput.getText()) && !Patterns.EMAIL_ADDRESS.matcher(binding.emailInput.getText()).matches()) {
                binding.emailContainer.setError(getResources().getString(R.string.email_not_valid));
            } else if (binding.phoneInput.getText() == null) {
                binding.phoneContainer.setError(getResources().getString(R.string.phone_number_empty));
            } else if (binding.phoneInput.getText().toString().trim().length() == 0) {
                binding.phoneContainer.setError(getResources().getString(R.string.phone_number_empty));
            } else if (binding.phoneInput.getText().toString().trim().length() < 12) {
                binding.phoneContainer.setError(getResources().getString(R.string.phone_number_incorrect));
            } else if (binding.commentInput.getText() == null) {
                binding.commentContainer.setError(getResources().getString(R.string.phone_number_empty));
            } else if (binding.commentInput.getText().toString().trim().length() == 0) {
                binding.commentContainer.setError(getResources().getString(R.string.phone_number_empty));
            } else {
                call_bookings(binding.nameInput.getText().toString(), binding.emailInput.getText().toString(), binding.phoneInput.getText().toString(), binding.commentInput.getText().toString());
                binding.nameInput.clearFocus();
                binding.emailInput.clearFocus();
                binding.phoneInput.clearFocus();
                binding.commentInput.clearFocus();
            }
        });

        binding.nameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence text, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence text, int start, int before, int count) {
                binding.nameContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable text) {
            }
        });

        binding.emailInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence text, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence text, int start, int before, int count) {
                binding.emailContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable text) {
            }
        });

        binding.phoneInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.phoneContainer.setError("");
                if (s.length() > 1 && !s.toString().substring(s.length() - 1).equals("-") && (s.length() == 3 || s.length() == 7) && binding.phoneInput.getText() != null)
                    binding.phoneInput.getText().append("-");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.commentInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence text, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence text, int start, int before, int count) {
                binding.commentContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable text) {
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Activity activity = (HomeScreen) context;
        try {
            appManageInterface = (AppManageInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement Interface");
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void call_bookings(String name, String email, String phone, String comment) {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("name", name);
                jsonObject.put("email", email);
                jsonObject.put("phone", phone);
                jsonObject.put("comment", comment);
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody call_bookings = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().call_bookings(call_bookings).enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_cancel)).setVisibility(View.GONE);
                            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                dialog.dismiss();
                                binding.nameInput.clearFocus();
                                binding.nameInput.setText("");
                                binding.emailInput.clearFocus();
                                binding.emailInput.setText("");
                                binding.phoneInput.clearFocus();
                                binding.phoneInput.setText("");
                                binding.commentInput.clearFocus();
                                binding.commentInput.setText("");

                            });
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            binding.noDataFound.setVisibility(View.GONE);
                            binding.loader.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.noDataFound.setVisibility(View.GONE);
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            binding.loader.setVisibility(View.GONE);
            binding.noDataFound.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    private void call_social_media() {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody call_social_media = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().call_social_media(call_social_media).enqueue(new Callback<SocialMediaResponse>() {
                @Override
                public void onResponse(@NonNull Call<SocialMediaResponse> call, @NonNull Response<SocialMediaResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {

                                instagramPath = response.body().getData().get(0).getInstagramLink();
                                fbPath = response.body().getData().get(0).getFacebookLink();
                                twitterPath = response.body().getData().get(0).getTwitterLink();
                                otherPath = response.body().getData().get(0).getOtherLink();
                                binding.loader.setVisibility(View.GONE);
                            } else {
                                binding.noDataFound.setVisibility(View.GONE);
                                binding.loader.setVisibility(View.GONE);
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            binding.noDataFound.setVisibility(View.GONE);
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<SocialMediaResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.noDataFound.setVisibility(View.GONE);
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            binding.loader.setVisibility(View.GONE);
            binding.noDataFound.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    private void getSliderData() {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody call_social_media = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().getBannerSlider(call_social_media).enqueue(new Callback<BannerSlider>() {
                @Override
                public void onResponse(@NonNull Call<BannerSlider> call, @NonNull Response<BannerSlider> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                bannerSliderData = response.body().getData();
                                adapterMainviewPager = new AdapterMainviewPager(context, bannerSliderData);
                                binding.mPager.setAdapter(adapterMainviewPager);
                                binding.mPager.setPagingEnabled(true);
                                binding.indicator.setViewPager(binding.mPager);
                                startImageSliderTimer();
                            } else {
                                binding.noDataSlider.setVisibility(View.GONE);
                            }
                        } else {
                            binding.noDataSlider.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<BannerSlider> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.noDataSlider.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            binding.noDataSlider.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    private void startImageSliderTimer() {
        synchronized (progressTimerSync) {
            if (progressTimer != null) {
                try {
                    progressTimer.cancel();
                    progressTimer = null;
                } catch (Exception e) {

                }
            }
            progressTimer = new Timer();
            progressTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    synchronized (sync) {
                        runOnUIThread(new Runnable() {
                            @Override
                            public void run() {

                                int currentItem = binding.mPager.getCurrentItem() + 1;
                                int totalItem = adapterMainviewPager.getCount();
                                if (currentItem == totalItem) {
                                    binding.mPager.setCurrentItem(0);
                                } else {
                                    binding.mPager.setCurrentItem(currentItem);
                                }
                            }
                        });
                    }
                }
            }, 0, 5000);
        }
    }

    public static void runOnUIThread(Runnable runnable) {
        runOnUIThread(runnable, 0);
    }

    public static void runOnUIThread(Runnable runnable, long delay) {
        if (delay == 0) {
            Backstage.applicationHandler.post(runnable);
        } else {
            Backstage.applicationHandler.postDelayed(runnable, delay);
        }
    }
}
